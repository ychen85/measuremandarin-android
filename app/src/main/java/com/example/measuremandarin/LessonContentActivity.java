package com.example.measuremandarin;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class LessonContentActivity extends AppCompatActivity implements View.OnClickListener {
    private LoadLesson loadLesson;
    private LoginDatabase loginDB;
    private AccountState accountState;
    private MediaPlayer mPlayer;
    RelativeLayout relativeLayout;
    ImageView imgLevelTitle;
    TextView txtNameCN;
    TextView txtNamePN;

    TextView txtMeaning;

    ImageView image1;
    TextView txtExample1;
    ImageView image2;
    TextView txtExample2;

    CheckBox checkCompleted;
    String id;
    String nameCN;
    String namePN;
    String use;
    String exampleCN1;
    String examplePN1;
    String exampleEN1;
    String exampleCN2;
    String examplePN2;
    String exampleEN2;
    ImageView imgLessonPre;
    ImageView imgLessonNext;
    ImageView imgList;
    Button btnPlay;
    int index;
    int level = loadLesson.getLevel();
    ArrayList<Lesson> currentList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_content);
        relativeLayout = findViewById(R.id.lessonContentLayout);
        loginDB = LoginDatabase.getInstance(this);
        accountState = AccountState.getInstance(this);
        loadLesson = LoadLesson.getInstance(this);
        imgLevelTitle = findViewById(R.id.imgLevelTitle);
        txtNameCN = findViewById(R.id.txtNameCN);
        txtNamePN = findViewById(R.id.txtNamePN);
        txtMeaning = findViewById(R.id.e_meaning);
        image1 = findViewById(R.id.image1);
        txtExample1 = findViewById(R.id.txtExample1);
        image2 = findViewById(R.id.image2);
        txtExample2 = findViewById(R.id.txtExample2);
        imgLessonPre = findViewById(R.id.imgLessonPre);
        imgLessonNext = findViewById(R.id.imgLessonNext);
        imgList = findViewById(R.id.imgGoToList);
        checkCompleted = findViewById(R.id.checkCompleted);
        btnPlay = findViewById(R.id.btnPlay);
//        lessonActive = loadLesson.getActiveLessonIndex();


        index = loadLesson.getActiveLessonIndex();


        //when click different level of lessons, display learning content with corresponded UI
        //and load the content from lessons.json
        switch (level) {  // 1 =easy, 2 = medium, 3 = hard

            case 1:
                currentList = loadLesson.getLessonEasyList();
                //change corresponded background for selected level.
                loadLesson.setActiveLesson(loadLesson.getLessonEasyList().get(index));
                relativeLayout.setBackgroundResource(R.drawable.lesson_easy_base);
                String nameEasyTitle = "easy_headline";
                int idEasyTitle = getResources().getIdentifier(nameEasyTitle, "drawable", getPackageName());
                Drawable drawableIDET = getResources().getDrawable(idEasyTitle);
                imgLevelTitle.setBackground(drawableIDET);

                nameCN = loadLesson.getLessonEasyList().get(index).getNameCN();
                namePN = loadLesson.getLessonEasyList().get(index).getNamePN();
                use = loadLesson.getLessonEasyList().get(index).getUse();
                exampleCN1 = loadLesson.getLessonEasyList().get(index).getExampleCN1();
                examplePN1 = loadLesson.getLessonEasyList().get(index).getExamplePN1();
                exampleEN1 = loadLesson.getLessonEasyList().get(index).getExampleEN1();
                exampleCN2 = loadLesson.getLessonEasyList().get(index).getExampleCN2();
                examplePN2 = loadLesson.getLessonEasyList().get(index).getExamplePN2();
                exampleEN2 = loadLesson.getLessonEasyList().get(index).getExampleEN2();

                txtNameCN.setText(nameCN);
                txtNamePN.setText(namePN);
                txtMeaning.setText(use);

                //change corresponded examples of each lesson
                txtExample1.setText(exampleCN1 + "\n" + examplePN1 + "\n" + exampleEN1);
                txtExample2.setText(exampleCN2 + "\n" + examplePN2 + "\n" + exampleEN2);


                //change example images for corresponded lessons
                String nameE1 = loadLesson.getLessonEasyList().get(index).getImg1();
                int idE1 = getResources().getIdentifier(nameE1, "drawable", getPackageName());
                Drawable drawableE1 = getResources().getDrawable(idE1);
                image1.setBackground(drawableE1);

                String nameE2 = loadLesson.getLessonEasyList().get(index).getImg2();
                int idE2 = getResources().getIdentifier(nameE2, "drawable", getPackageName());
                Drawable drawableE2 = getResources().getDrawable(idE2);
                image2.setBackground(drawableE2);
                break;

            case 2:
                currentList = loadLesson.getLessonMediumList();
                loadLesson.setActiveLesson(loadLesson.getLessonMediumList().get(index));
                relativeLayout.setBackgroundResource(R.drawable.lesson_medium_base);

                String nameMediumTitle = "medium_headline";
                int idMediumTitle = getResources().getIdentifier(nameMediumTitle, "drawable", getPackageName());
                Drawable drawableIDMT = getResources().getDrawable(idMediumTitle);
                imgLevelTitle.setBackground(drawableIDMT);

//                index = loadLesson.getActiveLessonIndex();

                nameCN = loadLesson.getLessonMediumList().get(index).getNameCN();
                namePN = loadLesson.getLessonMediumList().get(index).getNamePN();
                use = loadLesson.getLessonMediumList().get(index).getUse();
                exampleCN1 = loadLesson.getLessonMediumList().get(index).getExampleCN1();
                examplePN1 = loadLesson.getLessonMediumList().get(index).getExamplePN1();
                exampleEN1 = loadLesson.getLessonMediumList().get(index).getExampleEN1();
                exampleCN2 = loadLesson.getLessonMediumList().get(index).getExampleCN2();
                examplePN2 = loadLesson.getLessonMediumList().get(index).getExamplePN2();
                exampleEN2 = loadLesson.getLessonMediumList().get(index).getExampleEN2();

                txtNameCN.setText(nameCN);
                txtNamePN.setText(namePN);
                txtMeaning.setText(use);
                txtExample1.setText(exampleCN1 + "\n" + examplePN1 + "\n" + exampleEN1);
                txtExample2.setText(exampleCN2 + "\n" + examplePN2 + "\n" + exampleEN2);

                String nameM1 = loadLesson.getLessonMediumList().get(index).getImg1();
                int idM1 = getResources().getIdentifier(nameM1, "drawable", getPackageName());
                Drawable drawableM1 = getResources().getDrawable(idM1);
                image1.setBackground(drawableM1);

                String nameM2 = loadLesson.getLessonMediumList().get(index).getImg2();
                int idM2 = getResources().getIdentifier(nameM2, "drawable", getPackageName());
                Drawable drawableM2 = getResources().getDrawable(idM2);
                image2.setBackground(drawableM2);

                break;

            case 3:
                currentList = loadLesson.getLessonHardList();
                loadLesson.setActiveLesson(loadLesson.getLessonHardList().get(index));
                relativeLayout.setBackgroundResource(R.drawable.lesson_hard_base);

                String nameHardTitle = "hard_headline";
                int idHardTitle = getResources().getIdentifier(nameHardTitle, "drawable", getPackageName());
                Drawable drawableIDHT = getResources().getDrawable(idHardTitle);
                imgLevelTitle.setBackground(drawableIDHT);

                //               index = loadLesson.getActiveLessonIndex();
                nameCN = loadLesson.getLessonHardList().get(index).getNameCN();
                namePN = loadLesson.getLessonHardList().get(index).getNamePN();
                use = loadLesson.getLessonHardList().get(index).getUse();
                exampleCN1 = loadLesson.getLessonHardList().get(index).getExampleCN1();
                examplePN1 = loadLesson.getLessonHardList().get(index).getExamplePN1();
                exampleEN1 = loadLesson.getLessonHardList().get(index).getExampleEN1();
                exampleCN2 = loadLesson.getLessonHardList().get(index).getExampleCN2();
                examplePN2 = loadLesson.getLessonHardList().get(index).getExamplePN2();
                exampleEN2 = loadLesson.getLessonHardList().get(index).getExampleEN2();

                txtNameCN.setText(nameCN);
                txtNamePN.setText(namePN);
                txtMeaning.setText(use);
                txtExample1.setText(exampleCN1 + "\n" + examplePN1 + "\n" + exampleEN1);
                txtExample2.setText(exampleCN2 + "\n" + examplePN2 + "\n" + exampleEN2);

                String nameH1 = loadLesson.getLessonHardList().get(index).getImg1();
                int idH1 = getResources().getIdentifier(nameH1, "drawable", getPackageName());
                Drawable drawableH1 = getResources().getDrawable(idH1);
                image1.setBackground(drawableH1);

                String nameH2 = loadLesson.getLessonHardList().get(index).getImg2();
                int idH2 = getResources().getIdentifier(nameH2, "drawable", getPackageName());
                Drawable drawableH2 = getResources().getDrawable(idH2);
                image2.setBackground(drawableH2);
                break;
        }


        checkCompleted.setChecked(loadLesson.getActiveLesson().isCompleted());

        System.out.println("original setting is  " + loadLesson.getActiveLesson().isCompleted());

    }


    public boolean checkCompleted() {
        boolean checked = false;
        switch (loadLesson.getActiveLesson().getDifficulty()) {
            case "E":
                Cursor easy = loginDB.lessonCompleted(accountState.getActiveUser().getUsername(), "E");
                if (easy == null) {
                    System.out.println("not completed");
                } else {
                    int num = easy.getCount();
                    if (easy.moveToFirst()) {
                        for (int i = 0; i <= num; i++) {
                            @SuppressLint("Range") String level = easy.getString(easy.getColumnIndex("level"));
                            @SuppressLint("Range") String id = easy.getString(easy.getColumnIndex("id"));
                            if (loadLesson.getActiveLesson().getDifficulty().equals(level) && (loadLesson.getActiveLesson().getLessonID().equals(id+1))) {
                                checked = true;
                            }
                        }
                    }
                }
                break;

            case "M":
                Cursor medium = loginDB.lessonCompleted(accountState.getActiveUser().getUsername(), "M");
                if (medium == null) {
                    System.out.println("not completed");
                } else {
                    int num = medium.getCount();
                    if (medium.moveToFirst()) {
                        for (int i = 0; i <= num; i++) {
                            @SuppressLint("Range") String level = medium.getString(medium.getColumnIndex("level"));
                            @SuppressLint("Range") String id = medium.getString(medium.getColumnIndex("id"));
                            if (loadLesson.getActiveLesson().getDifficulty().equals(level) && (loadLesson.getActiveLesson().getLessonID().equals(id+1))) {
                                checked = true;
                            }
                        }
                    }
                }
                break;

            case "H":
                Cursor hard = loginDB.lessonCompleted(accountState.getActiveUser().getUsername(), "H");
                if (hard == null) {
                    System.out.println("not completed");
                } else {
                    int num = hard.getCount();
                    if (hard.moveToFirst()) {
                        for (int i = 0; i <= num; i++) {
                            @SuppressLint("Range") String level = hard.getString(hard.getColumnIndex("level"));
                            @SuppressLint("Range") String id = hard.getString(hard.getColumnIndex("id"));
                            if (loadLesson.getActiveLesson().getDifficulty().equals(level) && (loadLesson.getActiveLesson().getLessonID().equals(id+1))) {
                                checked = true;
                            }
                        }
                    }
                }
                break;
        }
        return checked;
    }




    @Override
    public void onClick(View view) {
        Intent intent;

        switch ( view.getId()){

            case R.id.btnPlay:

                String audioName = loadLesson.getActiveLesson().getAudio();
                int audio = getResources().getIdentifier(audioName,"raw",getPackageName());
                mPlayer = MediaPlayer.create(this, audio);
                mPlayer.start();
                break;
            case R.id.imgLessonPre:
                loadLesson.setActiveLessonIndex(index - 1);

                while(index <= 0){
                    Toast.makeText(this, "This is the first lesson of current level", Toast.LENGTH_SHORT).show();
                    return;
                }
                intent = getIntent();
                finish();
                startActivity(intent);
                this.overridePendingTransition(R.anim.leftin,R.anim.rightout);
                break;


            case R.id.imgLessonNext:
                loadLesson.setActiveLessonIndex(index +1);
                while(index >= currentList.size()-1){
                    Toast.makeText(this, "This is the last lesson of current level", Toast.LENGTH_SHORT).show();
                    return;
                }
                intent = getIntent();
                finish();
                startActivity(intent);
                this.overridePendingTransition(R.anim.rightin,R.anim.leftout);
                break;
            case R.id.imgGoToList:
                intent = new Intent(LessonContentActivity.this, LessonListActivity.class);
                startActivity(intent);
                break;

            case R.id.checkCompleted:

                System.out.println( " Original: "+ loadLesson.getActiveLesson().isCompleted());
                System.out.println("Current  status  lesson is   : " + checkCompleted.isChecked());

                if (checkCompleted.isChecked() == true){

                    loginDB.saveLessonProgress(loadLesson.getActiveLesson().getDifficulty(),loadLesson.getActiveLesson().getLessonID(),accountState.getActiveUser().getUsername());

                    //testing
                    Cursor cursor = loginDB.lessonCompleted(accountState.getActiveUser().getUsername(),loadLesson.getActiveLesson().getDifficulty());
                    if (cursor == null){
                        System.out.println("no lesson saved");
                    }
                    else {
                        int num = cursor.getCount();
                        System.out.println("completed lessons are " + num);
                    }


                    //insert into DB
                    loadLesson.getActiveLesson().setCompleted(true);
                    System.out.println( loadLesson.getActiveLesson().getDifficulty()+loadLesson.getActiveLesson().getLessonID()+ "Completed");
                    System.out.println(loadLesson.getActiveLesson().isCompleted());


                }else if (checkCompleted.isChecked() !=true){
                    // after click, it is mark not complete, remove data from DB

                    loginDB.removeLessonProgress(loadLesson.getActiveLesson().getDifficulty(),loadLesson.getActiveLesson().getLessonID(),accountState.getActiveUser().getUsername());
                    loadLesson.getActiveLesson().setCompleted(false);
                    System.out.println( loadLesson.getActiveLesson().getDifficulty()+loadLesson.getActiveLesson().getLessonID()+ "is not completed");
                }
                break;
        }
    }
}