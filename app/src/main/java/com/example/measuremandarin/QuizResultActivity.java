package com.example.measuremandarin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class QuizResultActivity extends AppCompatActivity {
    private AccountState accountState;
    private LoadQuiz loadQuiz;
    private LoadLesson loadLesson;
    private LoginDatabase loginDB;

    TextView txtResultLevel;
    TextView txtResult;
    TextView txtFeedback;
    TextView txtReact;
    Button btnQResultL;
    Button btnQResultQ;
    Button btnQResultA;
    Button btnQResultO;

    int result;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_result);
        accountState = AccountState.getInstance(this);
        loadQuiz = LoadQuiz.getInstance(this);
        loadLesson = LoadLesson.getInstance(this);
        loginDB = LoginDatabase.getInstance(this);

        result = loadQuiz.getCurPoint();

        txtResultLevel = findViewById(R.id.txtResultLevel);
        txtResult = findViewById(R.id.txtResult4);
        txtFeedback = findViewById(R.id.txtFeedback);
        txtReact = findViewById(R.id.txtReact);

        btnQResultL = findViewById(R.id.btnQResultL);
        btnQResultQ = findViewById(R.id.btnQResultQ);
        btnQResultA = findViewById(R.id.btnQResultA);
        btnQResultO = findViewById(R.id.btnQResultO);

        String level = loadQuiz.getActiveQuiz().getLevel();

        switch (level){
            case "Easy":
                String nameEasyTitle = "easy_headline";
                int idEasyTitle = getResources().getIdentifier(nameEasyTitle, "drawable", getPackageName());
                Drawable drawableIDET = getResources().getDrawable(idEasyTitle);
                txtResultLevel.setBackground(drawableIDET);

            case "Medium":

                String nameMediumTitle = "medium_headline";
                int idMediumTitle = getResources().getIdentifier(nameMediumTitle, "drawable", getPackageName());
                Drawable drawableIDMT = getResources().getDrawable(idMediumTitle);
                txtResultLevel.setBackground(drawableIDMT);

            case "Hard":
                String nameHardTitle = "hard_headline";
                int idHardTitle = getResources().getIdentifier(nameHardTitle, "drawable", getPackageName());
                Drawable drawableIDHT = getResources().getDrawable(idHardTitle);
                txtResultLevel.setBackground(drawableIDHT);

        }




        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MMM-dd hh:mm:ss");

        String strDate = dateFormat.format(date);

        //present final points
        String printResult = String.valueOf(result);

        //save date and result in SQLite database

        if (result > 7) {
            loginDB.saveResult(strDate, accountState.getActiveUser().getUsername(), printResult, loadQuiz.getActiveQuiz().getLevel(), "1");
        }else{
            loginDB.saveResult(strDate, accountState.getActiveUser().getUsername(), printResult, loadQuiz.getActiveQuiz().getLevel(), "0");
        }
        loginDB.resultHistory(accountState.getActiveUser().getUsername());
        System.out.println("quiz result saved");

        txtResult.setText(printResult +"correct answers out of"+ 12 + " questions");

        if (result > 7) {
            String difficulty = loadQuiz.getActiveQuiz().getLevel();
            if (difficulty.equals("Easy")) {

                txtFeedback.setText("Well Done!! \n You are ready for Medium level.");
                txtReact.setText("Learn Medium Level");
                accountState.setEasyDone(true);
            } else if (difficulty.equals("Medium")) {
                accountState.setMedDone(true);
                txtFeedback.setText("Well Done!! \n You are ready for Hard level.");
                txtReact.setText("Learn Hard Level");
            }else if (difficulty.equals("Hard")){
                accountState.setHardDone(true);
                txtFeedback.setText("Well Done!! \n You've reached native speaker level!!!");
            }
        } else if (result >= 5 && result <= 7) {
            txtFeedback.setText("So close to speak like native speaker. Practice more!");
            txtReact.setText("Try Again");
        } else if (result < 5) {
            txtFeedback.setText(strDate+"Try review the lessons again. Practice makes perfect!");
            txtReact.setText("Try Again");
        }



    }

    public void onClick(View view) {

        Intent intent;

        switch (view.getId()) {

            //menu button bar
            case R.id.btnQResultL:
                loadQuiz.setCurPoint(0);
                intent = new Intent(QuizResultActivity.this, LessonEnterActivity.class);
                startActivity(intent);
                break;

            case R.id.btnQResultQ:
                loadQuiz.setCurPoint(0);
                intent = new Intent(QuizResultActivity.this, QuizListActivity.class);
                startActivity(intent);
                break;

            case R.id.btnQResultA:
                loadQuiz.setCurPoint(0);
                intent = new Intent(QuizResultActivity.this, AccountActivity.class);
                startActivity(intent);
                break;

            case R.id.btnQResultO:
                loadQuiz.setCurPoint(0);
                intent = new Intent(QuizResultActivity.this, MainActivity.class);
                startActivity(intent);
                break;

            case R.id.txtReact:

                if(txtReact.getText().equals("Learn Medium Level")){
                    LoadLesson.setLevel(2);
                    intent = new Intent(QuizResultActivity.this, LessonContentActivity.class);
                    startActivity(intent);

                } else if (txtReact.getText().equals("Learn Hard Level")){

                    LoadLesson.setLevel(3);
                    intent = new Intent(QuizResultActivity.this, LessonContentActivity.class);
                    startActivity(intent);

                } else if (txtReact.getText().equals("Try Again")){
                    ArrayList<Quiz> allFailed = new ArrayList<>();
                    loadQuiz.setFailed(allFailed);
                    System.out.println("empty failed question for new quiz");
                    loadQuiz.setCurPoint(0);
                    System.out.println("Current point set to 0");
                    loadQuiz.setActiveQuizIndex(0);
                    System.out.println("display quiz index set to 0");
                    intent = new Intent(QuizResultActivity.this, QuizListActivity.class);
                    startActivity(intent);

                }
                break;


        }
    }
}