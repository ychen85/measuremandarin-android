package com.example.measuremandarin;

import android.graphics.drawable.Drawable;

public class Lesson {
    private String lessonID;
    private String difficulty;
    private String nameCN;
    private String namePN;
    private String use;
    private String exampleCN1;
    private String examplePN1;
    private String exampleEN1;
    private String exampleCN2;
    private String examplePN2;
    private String exampleEN2;
    private String img1;
    private String img2;
    private String audio;
    private boolean isCompleted = false;

    public Lesson(String lessonID, String difficulty, String nameCN, String namePN, String use,
                  String exampleCN1, String examplePN1, String exampleEN1, String exampleCN2,
                  String examplePN2, String exampleEN2, String img1, String img2, String audio) {
        this.lessonID = lessonID;
        this.difficulty = difficulty;
        this.nameCN = nameCN;
        this.namePN = namePN;
        this.use = use;
        this.exampleCN1 = exampleCN1;
        this.examplePN1 = examplePN1;
        this.exampleEN1 = exampleEN1;
        this.exampleCN2 = exampleCN2;
        this.examplePN2 = examplePN2;
        this.exampleEN2 = exampleEN2;
        this.img1 = img1;
        this.img2 = img2;
        this.audio = audio;
    }

    public String getLessonID() {
        return lessonID;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public String getNameCN() {
        return nameCN;
    }

    public String getNamePN() {
        return namePN;
    }

    public String getUse() {
        return use;
    }

    public String getExampleCN1() {
        return exampleCN1;
    }

    public String getExamplePN1() {
        return examplePN1;
    }

    public String getExampleEN1() {
        return exampleEN1;
    }

    public String getExampleCN2() {
        return exampleCN2;
    }

    public String getExamplePN2() {
        return examplePN2;
    }

    public String getExampleEN2() {
        return exampleEN2;
    }

    public String getImg1() {
        return img1;
    }

    public String getImg2() {
        return img2;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    public String getAudio() {
        return audio;
    }

}

