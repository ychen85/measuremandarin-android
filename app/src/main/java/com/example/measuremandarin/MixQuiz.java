package com.example.measuremandarin;

public class MixQuiz extends Quiz{

    private String optionEN1;
    private String optionEN2;
    private String optionEN3;
    private String optionEN4;

    public MixQuiz(String qid, String level, String testWordCN, String type,String imgInfo,
                   String target, String question, String answer,
                   String optionCN1, String optionPN1, String optionEN1,
                   String optionCN2, String optionPN2, String optionEN2,
                   String optionCN3, String optionPN3, String optionEN3,
                   String optionCN4, String optionPN4, String optionEN4,String src) {
        super(qid, level, testWordCN, target, type, imgInfo, question, answer, optionCN1, optionPN1, optionCN2, optionPN2, optionCN3, optionPN3, optionCN4, optionPN4, src);
        this.optionEN1 = optionEN1;
        this.optionEN2 = optionEN2;
        this.optionEN3 = optionEN3;
        this.optionEN4 = optionEN4;
    }

    public String getOptionEN1() {
        return optionEN1;
    }

    public void setOptionEN1(String optionEN1) {
        this.optionEN1 = optionEN1;
    }

    public String getOptionEN2() {
        return optionEN2;
    }

    public void setOptionEN2(String optionEN2) {
        this.optionEN2 = optionEN2;
    }

    public String getOptionEN3() {
        return optionEN3;
    }

    public void setOptionEN3(String optionEN3) {
        this.optionEN3 = optionEN3;
    }

    public String getOptionEN4() {
        return optionEN4;
    }

    public void setOptionEN4(String optionEN4) {
        this.optionEN4 = optionEN4;
    }
}
