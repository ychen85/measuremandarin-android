package com.example.measuremandarin;

import android.content.Context;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class LoadQuiz {
    private static LoadQuiz instance;
    private ArrayList<Quiz> quizEasyList;
    private ArrayList<Quiz> quizMediumList;
    private ArrayList<Quiz> quizHardList;
    private ArrayList<Quiz> failed ;
    private ArrayList<MixQuiz> mixEasy;
    private ArrayList<Quiz> mixMedium;
    private ArrayList<MixQuiz> mixHard;

    private ArrayList<Quiz> curQuizList;
    private Quiz activeQuiz;
    private int activeQuizIndex;
    private int level; // 1 = easy , 2 = medium , 3 = hard
    private int curPoint;
    boolean didFailed = false;



    public static LoadQuiz getInstance(Context context){
        if(instance == null) {
            instance = new LoadQuiz(context);
        }
        return instance;
    }

    private LoadQuiz(Context context) {

        quizEasyList = new ArrayList<>();
        quizMediumList = new ArrayList<>();
        quizHardList = new ArrayList<>();
        failed = new ArrayList<>();
        mixEasy = new ArrayList<>();
        mixMedium  = new ArrayList<>();
        mixHard = new ArrayList<>();

        curQuizList = new ArrayList<>();
        activeQuizIndex = 0;
        curPoint = 0;

        // load json file into string
        InputStream is = context.getResources().openRawResource(R.raw.quiz);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String jsonString = writer.toString();





        JSONParser parser = new JSONParser();
        try {
            //Object object = parser.parse(new FileReader("raw/users.json"));
            Object object = parser.parse(jsonString);

            //convert Object to JSONObject
            JSONObject jsonObject = (JSONObject) object;

            //Reading the array
            JSONArray easyJSONArray = (JSONArray) jsonObject.get("quizEasy");
            JSONObject easyJSONObject;

            //Create the objects
            for (Object quiz : easyJSONArray) {
                easyJSONObject = (JSONObject) quiz;
                quizEasyList.add(new Quiz(
                        ((JSONObject) quiz).get("qID").toString(),
                        ((JSONObject) quiz).get("level").toString(),
                        ((JSONObject) quiz).get("testWordCN").toString(),
                        ((JSONObject) quiz).get("target").toString(),
                        ((JSONObject) quiz).get("type").toString(),
                        ((JSONObject) quiz).get("imgInfo").toString(),
                        ((JSONObject) quiz).get("question").toString(),
                        ((JSONObject) quiz).get("answer").toString(),
                        ((JSONObject) quiz).get("optionCN1").toString(),
                        ((JSONObject) quiz).get("optionPN1").toString(),
                        ((JSONObject) quiz).get("optionCN2").toString(),
                        ((JSONObject) quiz).get("optionPN2").toString(),
                        ((JSONObject) quiz).get("optionCN3").toString(),
                        ((JSONObject) quiz).get("optionPN3").toString(),
                        ((JSONObject) quiz).get("optionCN4").toString(),
                        ((JSONObject) quiz).get("optionPN4").toString(),
                        ((JSONObject) quiz).get("src").toString()));
            }



            //Reading the array
            JSONArray mediumJSONArray = (JSONArray) jsonObject.get("quizMedium");
            JSONObject mediumJSONObject;

            //Create the objects
            for (Object quiz : mediumJSONArray) {
                mediumJSONObject = (JSONObject) quiz;
                quizMediumList.add(new Quiz(
                        ((JSONObject) quiz).get("qID").toString(),
                        ((JSONObject) quiz).get("level").toString(),
                        ((JSONObject) quiz).get("testWordCN").toString(),
                        ((JSONObject) quiz).get("target").toString(),
                        ((JSONObject) quiz).get("type").toString(),
                        ((JSONObject) quiz).get("imgInfo").toString(),
                        ((JSONObject) quiz).get("question").toString(),
                        ((JSONObject) quiz).get("answer").toString(),
                        ((JSONObject) quiz).get("optionCN1").toString(),
                        ((JSONObject) quiz).get("optionPN1").toString(),
                        ((JSONObject) quiz).get("optionCN2").toString(),
                        ((JSONObject) quiz).get("optionPN2").toString(),
                        ((JSONObject) quiz).get("optionCN3").toString(),
                        ((JSONObject) quiz).get("optionPN3").toString(),
                        ((JSONObject) quiz).get("optionCN4").toString(),
                        ((JSONObject) quiz).get("optionPN4").toString(),
                        ((JSONObject) quiz).get("src").toString()));
            }

            //Reading the array
            JSONArray hardJSONArray = (JSONArray) jsonObject.get("quizHard");
            JSONObject hardJSONObject;

            for (Object quiz : hardJSONArray) {
                hardJSONObject = (JSONObject) quiz;
                quizHardList.add(new Quiz(
                        ((JSONObject) quiz).get("qID").toString(),
                        ((JSONObject) quiz).get("level").toString(),
                        ((JSONObject) quiz).get("testWordCN").toString(),
                        ((JSONObject) quiz).get("target").toString(),
                        ((JSONObject) quiz).get("type").toString(),
                        ((JSONObject) quiz).get("imgInfo").toString(),
                        ((JSONObject) quiz).get("question").toString(),
                        ((JSONObject) quiz).get("answer").toString(),
                        ((JSONObject) quiz).get("optionCN1").toString(),
                        ((JSONObject) quiz).get("optionPN1").toString(),
                        ((JSONObject) quiz).get("optionCN2").toString(),
                        ((JSONObject) quiz).get("optionPN2").toString(),
                        ((JSONObject) quiz).get("optionCN3").toString(),
                        ((JSONObject) quiz).get("optionPN3").toString(),
                        ((JSONObject) quiz).get("optionCN4").toString(),
                        ((JSONObject) quiz).get("optionPN4").toString(),
                        ((JSONObject) quiz).get("src").toString()));
            }


            //Reading the array
            JSONArray mixEasyJSONArray = (JSONArray) jsonObject.get("mixQuizE");
            JSONObject mixEasyJSONObject;

            for (Object quiz : mixEasyJSONArray) {
                mixEasyJSONObject = (JSONObject) quiz;
                mixEasy.add(new MixQuiz(
                        ((JSONObject) quiz).get("qID").toString(),
                        ((JSONObject) quiz).get("level").toString(),
                        ((JSONObject) quiz).get("testWordCN").toString(),
                        ((JSONObject) quiz).get("target").toString(),
                        ((JSONObject) quiz).get("type").toString(),
                        ((JSONObject) quiz).get("imgInfo").toString(),
                        ((JSONObject) quiz).get("question").toString(),
                        ((JSONObject) quiz).get("answer").toString(),
                        ((JSONObject) quiz).get("optionCN1").toString(),
                        ((JSONObject) quiz).get("optionPN1").toString(),
                        ((JSONObject) quiz).get("optionEN1").toString(),
                        ((JSONObject) quiz).get("optionCN2").toString(),
                        ((JSONObject) quiz).get("optionPN2").toString(),
                        ((JSONObject) quiz).get("optionEN2").toString(),
                        ((JSONObject) quiz).get("optionCN3").toString(),
                        ((JSONObject) quiz).get("optionPN3").toString(),
                        ((JSONObject) quiz).get("optionEN3").toString(),
                        ((JSONObject) quiz).get("optionCN4").toString(),
                        ((JSONObject) quiz).get("optionPN4").toString(),
                        ((JSONObject) quiz).get("optionEN4").toString(),
                        ((JSONObject) quiz).get("src").toString()));
                System.out.println(mixEasy.size());

            }


            //Reading the array
            JSONArray mixMediumJSONArray = (JSONArray) jsonObject.get("mixQuizM");
            JSONObject mixMediumJSONObject;

            for (Object quiz : mixMediumJSONArray) {
                mixMediumJSONObject = (JSONObject) quiz;
                mixMedium.add(new MixQuiz(
                        ((JSONObject) quiz).get("qID").toString(),
                        ((JSONObject) quiz).get("level").toString(),
                        ((JSONObject) quiz).get("testWordCN").toString(),
                        ((JSONObject) quiz).get("target").toString(),
                        ((JSONObject) quiz).get("type").toString(),
                        ((JSONObject) quiz).get("imgInfo").toString(),
                        ((JSONObject) quiz).get("question").toString(),
                        ((JSONObject) quiz).get("answer").toString(),
                        ((JSONObject) quiz).get("optionCN1").toString(),
                        ((JSONObject) quiz).get("optionPN1").toString(),
                        ((JSONObject) quiz).get("optionEN1").toString(),
                        ((JSONObject) quiz).get("optionCN2").toString(),
                        ((JSONObject) quiz).get("optionPN2").toString(),
                        ((JSONObject) quiz).get("optionEN2").toString(),
                        ((JSONObject) quiz).get("optionCN3").toString(),
                        ((JSONObject) quiz).get("optionPN3").toString(),
                        ((JSONObject) quiz).get("optionEN3").toString(),
                        ((JSONObject) quiz).get("optionCN4").toString(),
                        ((JSONObject) quiz).get("optionPN4").toString(),
                        ((JSONObject) quiz).get("optionEN4").toString(),
                        ((JSONObject) quiz).get("src").toString()));
                System.out.println(mixMedium.size());
            }




            //Reading the array
            JSONArray mixHardJSONArray = (JSONArray) jsonObject.get("mixQuizH");
            JSONObject mixHardJSONObject;

            for (Object quiz : mixHardJSONArray) {
                mixHardJSONObject = (JSONObject) quiz;
                mixHard.add(new MixQuiz(
                        ((JSONObject) quiz).get("qID").toString(),
                        ((JSONObject) quiz).get("level").toString(),
                        ((JSONObject) quiz).get("testWordCN").toString(),
                        ((JSONObject) quiz).get("target").toString(),
                        ((JSONObject) quiz).get("type").toString(),
                        ((JSONObject) quiz).get("imgInfo").toString(),
                        ((JSONObject) quiz).get("question").toString(),
                        ((JSONObject) quiz).get("answer").toString(),
                        ((JSONObject) quiz).get("optionCN1").toString(),
                        ((JSONObject) quiz).get("optionPN1").toString(),
                        ((JSONObject) quiz).get("optionEN1").toString(),
                        ((JSONObject) quiz).get("optionCN2").toString(),
                        ((JSONObject) quiz).get("optionPN2").toString(),
                        ((JSONObject) quiz).get("optionEN2").toString(),
                        ((JSONObject) quiz).get("optionCN3").toString(),
                        ((JSONObject) quiz).get("optionPN3").toString(),
                        ((JSONObject) quiz).get("optionEN3").toString(),
                        ((JSONObject) quiz).get("optionCN4").toString(),
                        ((JSONObject) quiz).get("optionPN4").toString(),
                        ((JSONObject) quiz).get("optionEN4").toString(),
                        ((JSONObject) quiz).get("src").toString()));
                System.out.println(mixHard.size());
            }

            System.out.println(mixEasy.toString());





        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public ArrayList<Quiz> getQuizEasyList() {
        return quizEasyList;
    }

    public void setQuizEasyList(ArrayList<Quiz> quizEasyList) {
        this.quizEasyList = quizEasyList;
    }

    public ArrayList<Quiz> getQuizMediumList() {
        return quizMediumList;
    }

    public void setQuizMediumList(ArrayList<Quiz> quizMediumList) {
        this.quizMediumList = quizMediumList;
    }

    public ArrayList<Quiz> getQuizHardList() {
        return quizHardList;
    }

    public void setQuizHardList(ArrayList<Quiz> quizHardList) {
        this.quizHardList = quizHardList;
    }

    public ArrayList<Quiz> getCurQuizList() {
        return curQuizList;
    }

    public void setCurQuizList(ArrayList<Quiz> curQuizList) {
        this.curQuizList = curQuizList;
    }

    public Quiz getActiveQuiz() {
        return activeQuiz;
    }

    public void setActiveQuiz(Quiz activeQuiz) {
        this.activeQuiz = activeQuiz;
    }

    public int getActiveQuizIndex() {
        return activeQuizIndex;
    }

    public void setActiveQuizIndex(int activeQuizIndex) {
        this.activeQuizIndex = activeQuizIndex;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getCurPoint() {
        return curPoint;
    }

    public void setCurPoint(int curPoint) {
        this.curPoint = curPoint;
    }

    public ArrayList<Quiz> getFailed() {
        return failed;
    }

    public void setFailed(ArrayList<Quiz> failed) {
        this.failed = failed;
    }

    public boolean isDidFailed() {
        return didFailed;
    }

    public void setDidFailed(boolean didFailed) {
        this.didFailed = didFailed;
    }


    public ArrayList<MixQuiz> getMixEasy() {
        return mixEasy;
    }

    public void setMixEasy(ArrayList<MixQuiz> mixEasy) {
        this.mixEasy = mixEasy;
    }

    public ArrayList<Quiz> getMixMedium() {
        return mixMedium;
    }

    public void setMixMedium(ArrayList<Quiz> mixMedium) {
        this.mixMedium = mixMedium;
    }

    public ArrayList<MixQuiz> getMixHard() {
        return mixHard;
    }

    public void setMixHard(ArrayList<MixQuiz> mixHard) {
        this.mixHard = mixHard;
    }
}





