package com.example.measuremandarin;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class QuizNoInputActivity extends AppCompatActivity {

    private TextView txtNoInputOption1;
    private TextView txtNoInputOption2;
    private TextView txtNoInputOption3;
    private TextView txtNoInputOption4;
    private TextView txtNoInputQuestion1;
    private TextView txtNoInputQuestion2;
    private ImageView imgEndNoInputQuiz;
    private ImageView imgQuizPre;
    private ImageView imgQuizNext;
    private  ImageView imgLevelNoInputQuiz;
    private ImageView imgQuizNoInput;


    private String noOptionCN1;
    private String noOptionPN1;
    private String noOptionEN1;
    private String noOptionCN2;
    private String noOptionPN2;
    private String noOptionEN2;
    private String noOptionCN3;
    private String noOptionPN3;
    private String noOptionEN3;
    private String noOptionCN4;
    private String noOptionPN4;
    private String noOptionEN4;

    int level;  // 1:easy 2: medium 3: hard 4:failed quiz
    private LoadQuiz loadQuiz;

    ArrayList<Quiz> displayQuiz;
    RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_noinput);
        relativeLayout = findViewById(R.id.quizDisplay);
        loadQuiz = LoadQuiz.getInstance(this);
        level = loadQuiz.getLevel();

        txtNoInputQuestion1 = findViewById(R.id.txtNoInputQuestion1);
        txtNoInputQuestion2 = findViewById(R.id.txtNoInputQuestion2);
        txtNoInputOption1 = findViewById(R.id.txtNoInputOption1);
        txtNoInputOption2 = findViewById(R.id.txtNoInputOption2);
        txtNoInputOption3 = findViewById(R.id.txtNoInputOption3);
        txtNoInputOption4 = findViewById(R.id.txtNoInputOption4);

        imgEndNoInputQuiz = findViewById(R.id.imgEndNoInputQuiz);

        imgQuizPre = findViewById(R.id.imgQuizPre);
        imgQuizNext =findViewById(R.id.imgQuizNext);

        imgLevelNoInputQuiz = findViewById(R.id.imgLevelNoInputQuiz);
        imgQuizNoInput = findViewById(R.id.imgQuizNoInput);



        switch (level) {
            case 1:
                relativeLayout.setBackgroundResource(R.drawable.quiz_easy_base);
                displayQuiz = genDisplayQuiz(loadQuiz.getQuizEasyList());  //generate questions for this quiz

                loadQuiz.setCurQuizList(displayQuiz);
                loadQuiz.setActiveQuiz(displayQuiz.get(loadQuiz.getActiveQuizIndex()));
                String nameEasyTitle = "easy_headline";
                int idEasyTitle = getResources().getIdentifier(nameEasyTitle, "drawable", getPackageName());
                Drawable drawableIDET = getResources().getDrawable(idEasyTitle);
                imgLevelNoInputQuiz.setBackground(drawableIDET);

                String nameEasyFrame = "btneasyquizframe";
                int easy = getResources().getIdentifier(nameEasyFrame, "drawable", getPackageName());
                Drawable drawableEasyF = getResources().getDrawable(easy);
                txtNoInputOption1.setBackground(drawableEasyF);
                txtNoInputOption2.setBackground(drawableEasyF);
                txtNoInputOption3.setBackground(drawableEasyF);
                txtNoInputOption4.setBackground(drawableEasyF);

                //quiz pic name get from src
                String name1 = loadQuiz.getCurQuizList().get(loadQuiz.getActiveQuizIndex()).getSrc();
                int id1 = getResources().getIdentifier(name1, "drawable", getPackageName());
                Drawable drawable1 = getResources().getDrawable(id1);
                imgQuizNoInput.setBackground(drawable1);
                break;


            case 2:
                relativeLayout.setBackgroundResource(R.drawable.quiz_medium_base);
                displayQuiz = genDisplayQuiz(loadQuiz.getQuizMediumList());
                loadQuiz.setCurQuizList(displayQuiz);
                loadQuiz.setActiveQuiz(displayQuiz.get(loadQuiz.getActiveQuizIndex()));
                String nameMediumTitle = "medium_headline";
                int idMediumTitle = getResources().getIdentifier(nameMediumTitle, "drawable", getPackageName());
                Drawable drawableIDMT = getResources().getDrawable(idMediumTitle);
                imgLevelNoInputQuiz.setBackground(drawableIDMT);


                String nameMediumFrame = "btnmediumquizframe";
                int medium = getResources().getIdentifier(nameMediumFrame, "drawable", getPackageName());
                Drawable drawableMediumF = getResources().getDrawable(medium);
                txtNoInputOption1.setBackground(drawableMediumF);
                txtNoInputOption2.setBackground(drawableMediumF);
                txtNoInputOption3.setBackground(drawableMediumF);
                txtNoInputOption4.setBackground(drawableMediumF);

                String name2 = loadQuiz.getCurQuizList().get(loadQuiz.getActiveQuizIndex()).getSrc();
                int id2 = getResources().getIdentifier(name2, "drawable", getPackageName());
                Drawable drawable2 = getResources().getDrawable(id2);
                imgQuizNoInput.setBackground(drawable2);
                break;


            case 3:
                relativeLayout.setBackgroundResource(R.drawable.quiz_hard_base);

                displayQuiz = genDisplayQuiz(loadQuiz.getQuizHardList());
                loadQuiz.setCurQuizList(displayQuiz);
                loadQuiz.setActiveQuiz(displayQuiz.get(loadQuiz.getActiveQuizIndex()));
                String nameHardTitle = "hard_headline";
                int idHardTitle = getResources().getIdentifier(nameHardTitle, "drawable", getPackageName());
                Drawable drawableIDHT = getResources().getDrawable(idHardTitle);
                imgLevelNoInputQuiz.setBackground(drawableIDHT);


                String nameHardFrame = "btnhardquizframe";
                int hard = getResources().getIdentifier(nameHardFrame, "drawable", getPackageName());
                Drawable drawableHardF = getResources().getDrawable(hard);
                txtNoInputOption1.setBackground(drawableHardF);
                txtNoInputOption2.setBackground(drawableHardF);
                txtNoInputOption3.setBackground(drawableHardF);
                txtNoInputOption4.setBackground(drawableHardF);

                String name3 = loadQuiz.getCurQuizList().get(loadQuiz.getActiveQuizIndex()).getSrc();
                int id3 = getResources().getIdentifier(name3, "drawable", getPackageName());
                Drawable drawable3 = getResources().getDrawable(id3);
                imgQuizNoInput.setBackground(drawable3);
                break;

        }
        System.out.println("current quiz ID : " + loadQuiz.getCurQuizList().get(loadQuiz.getActiveQuizIndex()).getQid());
        noOptionCN1 = displayQuiz.get(loadQuiz.getActiveQuizIndex()).getOptionCN1();
        noOptionPN1 = displayQuiz.get(loadQuiz.getActiveQuizIndex()).getOptionPN1();

        noOptionCN2 = displayQuiz.get(loadQuiz.getActiveQuizIndex()).getOptionCN2();
        noOptionPN2 = displayQuiz.get(loadQuiz.getActiveQuizIndex()).getOptionPN2();
        noOptionCN3 = displayQuiz.get(loadQuiz.getActiveQuizIndex()).getOptionCN3();
        noOptionPN3 = displayQuiz.get(loadQuiz.getActiveQuizIndex()).getOptionPN3();
        noOptionCN4 = displayQuiz.get(loadQuiz.getActiveQuizIndex()).getOptionCN4();
        noOptionPN4 = displayQuiz.get(loadQuiz.getActiveQuizIndex()).getOptionPN4();


        txtNoInputQuestion1.setText(displayQuiz.get(loadQuiz.getActiveQuizIndex()).getImgInfo());
        txtNoInputQuestion2.setText(displayQuiz.get(loadQuiz.getActiveQuizIndex()).getQuestion());

        if (loadQuiz.getActiveQuiz().getClass().equals(MixQuiz.class)) {
            MixQuiz temp = (MixQuiz)loadQuiz.getActiveQuiz();
            noOptionEN1 = temp.getOptionEN1();
            noOptionEN2 = temp.getOptionEN2();
            noOptionEN3 = temp.getOptionEN3();
            noOptionEN4 = temp.getOptionEN4();

            txtNoInputOption1.setText("A : " + noOptionCN1 + "\n" + noOptionPN1 + "\n" + noOptionEN1 );
            txtNoInputOption2.setText("B : " + noOptionCN2 + "\n" + noOptionPN2 + "\n" + noOptionEN2 );
            txtNoInputOption3.setText("C : " + noOptionCN3 + "\n" + noOptionPN3 + "\n" + noOptionEN3 );
            txtNoInputOption4.setText("D : " + noOptionCN4 + "\n" + noOptionPN4 + "\n" + noOptionEN4 );

        } else {
            txtNoInputOption1.setText("A : " + noOptionCN1 + "\n" + noOptionPN1);
            txtNoInputOption2.setText("B : " + noOptionCN2 + "\n" + noOptionPN2);
            txtNoInputOption3.setText("C : " + noOptionCN3 + "\n" + noOptionPN3);
            txtNoInputOption4.setText("D : " + noOptionCN4 + "\n" + noOptionPN4);

        }
    }

    public void onClick(View view){
        Intent intent;

        switch (view.getId()){
            case R.id.imgEndNoInputQuiz:
                Toast.makeText(this, "stop quiz!", Toast.LENGTH_SHORT).show();
                intent = new Intent(QuizNoInputActivity.this, QuizListActivity.class);
                startActivity(intent);

                break;

            case R.id.imgQuizPre:
                loadQuiz.setActiveQuizIndex((loadQuiz.getActiveQuizIndex()-1));

                while ( loadQuiz.getActiveQuizIndex() < 0){
                    Toast.makeText(this, "This is the first question!! ", Toast.LENGTH_SHORT).show();
                    return;
                }
                intent = getIntent();
                finish();
                startActivity(intent);
                this.overridePendingTransition(R.anim.leftin,R.anim.rightout);
                this.overridePendingTransition(R.anim.rightin,R.anim.leftout);
                break;

            case R.id.imgQuizNext:
                loadQuiz.setActiveQuizIndex((loadQuiz.getActiveQuizIndex()+1));

                while ( loadQuiz.getActiveQuizIndex() >= loadQuiz.getCurQuizList().size()){
                    Toast.makeText(this, "This is the last question!! ", Toast.LENGTH_SHORT).show();
                    return;
                }
                intent = getIntent();
                finish();
                startActivity(intent);
                this.overridePendingTransition(R.anim.rightin,R.anim.leftout);
                break;

        }
    }


    //generate a quiz with 2 formats (single/mix) of quesitons for the user
    public ArrayList<Quiz> genDisplayQuiz(ArrayList<Quiz> a){
        ArrayList<Quiz> display = new ArrayList<>();

        switch (loadQuiz.getLevel()){

            case 1:
                //the list hold different questions, aiming at the same target word knowledge(E1.E2..etc)
                ArrayList<Quiz> E1 = new ArrayList<>();
                ArrayList<Quiz> E2 = new ArrayList<>();
                ArrayList<Quiz> E3 = new ArrayList<>();
                ArrayList<Quiz> E4 = new ArrayList<>();

                //get all easy level questions arraylist from JSON
                // group them in different target word
                for (Quiz q : a) {
                    if (q.getTarget().equals("E1")) {
                        E1.add(q);
                    } else if (q.getTarget().equals("E2")) {
                        E2.add(q);
                    } else if (q.getTarget().equals("E3")) {
                        E3.add(q);
                    } else if (q.getTarget().equals("E4")) {
                        E4.add(q);
                    }
                }

                //random select two questions to test 1 target word, in the same level
                Random r1 = new Random();
                int e1 = r1.nextInt(1);
                int e2 = r1.nextInt(3)+1;

                display.add(E1.get(e1));
                display.add(E2.get(e1));
                display.add(E3.get(e1));
                display.add(E4.get(e1));
                display.add(E1.get(e2));
                display.add(E2.get(e2));
                display.add(E3.get(e2));
                display.add(E4.get(e2));

                //add in questions that test a couple of target words in the same question
                display.addAll(loadQuiz.getMixEasy());
                //shuffle the whole list after all questions are selected to be displayed
                Collections.shuffle(display);
                System.out.println("!!standard quiz size is : " + display.size());
                break;

            case 2:
                ArrayList<Quiz> M1 = new ArrayList<>();
                ArrayList<Quiz> M2 = new ArrayList<>();
                ArrayList<Quiz> M3 = new ArrayList<>();
                ArrayList<Quiz> M4 = new ArrayList<>();

                for (Quiz q : a) {
                    if (q.getTarget().equals("M1")) {
                        M1.add(q);
                    } else if (q.getTarget().equals("M2")) {
                        M2.add(q);
                    } else if (q.getTarget().equals("M3")) {
                        M3.add(q);
                    } else if (q.getTarget().equals("M4")) {
                        M4.add(q);
                    }
                }

                Random r2 = new Random();
                int m1 = r2.nextInt(1);
                int m2 = r2.nextInt(3)+1;

                display.add(M1.get(m1));
                display.add(M2.get(m1));
                display.add(M3.get(m1));
                display.add(M4.get(m1));
                display.add(M1.get(m2));
                display.add(M2.get(m2));
                display.add(M3.get(m2));
                display.add(M4.get(m2));



                //add in questions that test a couple of target words in the same question
                display.addAll(loadQuiz.getMixMedium());
                //shuffle the whole list after all questions are selected to be displayed
                Collections.shuffle(display);

                System.out.println("standard quiz size is : " + display.size());
                break;

            case 3:
                ArrayList<Quiz> H1 = new ArrayList<>();
                ArrayList<Quiz> H2 = new ArrayList<>();
                ArrayList<Quiz> H3 = new ArrayList<>();
                ArrayList<Quiz> H4 = new ArrayList<>();

                for (Quiz q : a) {
                    if (q.getTarget().equals("H1")) {
                        H1.add(q);
                    } else if (q.getTarget().equals("H2")) {
                        H2.add(q);
                    } else if (q.getTarget().equals("H3")) {
                        H3.add(q);
                    } else if (q.getTarget().equals("H4")) {
                        H4.add(q);
                    }
                }

                Random r3 = new Random();
                int h1 = r3.nextInt(1);
                int h2 = r3.nextInt(3)+1;

                display.add(H1.get(h1));
                display.add(H2.get(h1));
                display.add(H3.get(h1));
                display.add(H4.get(h1));
                display.add(H1.get(h2));
                display.add(H2.get(h2));
                display.add(H3.get(h2));
                display.add(H4.get(h2));

                //add in questions that test a couple of target words in the same question
                display.addAll(loadQuiz.getMixHard());
                //shuffle the whole list after all questions are selected to be displayed
                Collections.shuffle(display);


                System.out.println("standard quiz size is : " + display.size());
                break;

        }
        return display;
    }

}