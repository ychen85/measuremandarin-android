package com.example.measuremandarin;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;

public class LessonListActivity extends AppCompatActivity {
    private AccountState accountState;
    private LoadLesson loadLesson;
    private LoginDatabase loginDB;
    TextView txtE1;
    TextView txtE2;
    TextView txtE3;
    TextView txtE4;
    TextView txtM1;
    TextView txtM2;
    TextView txtM3;
    TextView txtM4;
    TextView txtH1;
    TextView txtH2;
    TextView txtH3;
    TextView txtH4;
    Button btnLessonL;
    Button btnLessonQ;
    Button btnLessonA;
    Button btnLessonO;
    Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_list);
        accountState = AccountState.getInstance(this);
        loadLesson = LoadLesson.getInstance(this);
        loginDB = LoginDatabase.getInstance(this);

        txtE1 = findViewById(R.id.txtE1);
        txtE2 = findViewById(R.id.txtE2);
        txtE3 = findViewById(R.id.txtE3);
        txtE4 = findViewById(R.id.txtE4);
        txtM1 = findViewById(R.id.txtM1);
        txtM2 = findViewById(R.id.txtM2);
        txtM3 = findViewById(R.id.txtM3);
        txtM4 = findViewById(R.id.txtM4);
        txtH1 = findViewById(R.id.txtH1);
        txtH2 = findViewById(R.id.txtH2);
        txtH3 = findViewById(R.id.txtH3);
        txtH4 = findViewById(R.id.txtH4);

        btnLessonL =findViewById(R.id.btnLessonL);
        btnLessonQ =findViewById(R.id.btnLessonQ);
        btnLessonA =findViewById(R.id.btnLessonA);
        btnLessonO =findViewById(R.id.btnLessonO);




        ArrayList<TextView> txtEasyList = new ArrayList<>();
        txtEasyList.add(txtE1);
        txtEasyList.add(txtE2);
        txtEasyList.add(txtE3);
        txtEasyList.add(txtE4);
        System.out.println("size of Textview Easy Lesson : " + txtEasyList.size());
        ArrayList<TextView> txtMedList = new ArrayList<>();
        txtMedList.add(txtM1);
        txtMedList.add(txtM2);
        txtMedList.add(txtM3);
        txtMedList.add(txtM4);
        System.out.println("size of Textview Med Lesson : " + txtMedList.size());
        ArrayList<TextView> txtHardList = new ArrayList<>();
        txtHardList.add(txtH1);
        txtHardList.add(txtH2);
        txtHardList.add(txtH3);
        txtHardList.add(txtH4);
        System.out.println("size of Textview Hard Lesson : " + txtHardList.size());
        loginDB.checkLesson(accountState.getActiveUser().getUsername());
        checkCompleted(accountState.getActiveUser().getUsername()); //assign lesson object isCompleted() value,

        // give correspondent appearance based on isCompleted() value
        for (int i = 0; i < txtEasyList.size();i++){
            txtEasyList.get(i).setText(loadLesson.getLessonEasyList().get(i).getNameCN()+ "\n" + loadLesson.getLessonEasyList().get(i).getNamePN());
            trackProgress(loadLesson.getLessonEasyList().get(i), txtEasyList.get(i));
        }

        for (int i =0; i < txtMedList.size();i++){
            txtMedList.get(i).setText(loadLesson.getLessonMediumList().get(i).getNameCN()+ "\n" + loadLesson.getLessonMediumList().get(i).getNamePN());
            trackProgress(loadLesson.getLessonMediumList().get(i), txtMedList.get(i));
        }
        for (int i =0; i < txtHardList.size();i++){
            txtHardList.get(i).setText(loadLesson.getLessonHardList().get(i).getNameCN()+ "\n" + loadLesson.getLessonHardList().get(i).getNamePN());
            trackProgress(loadLesson.getLessonHardList().get(i), txtHardList.get(i));
        }

    }


    //change lesson icon color if it is completed
    public void trackProgress(Lesson l, TextView txt){
        String level = l.getDifficulty();
        switch (level){
            case "E":
                if(l.isCompleted() == true){
                    String ebg = "greenlightbg";
                    int easy = getResources().getIdentifier(ebg, "drawable", getPackageName());
                    Drawable easybg = getResources().getDrawable(easy);
                    txt.setBackground(easybg);
                }else{
                    String eg = "greylightbg";
                    int e = getResources().getIdentifier(eg, "drawable", getPackageName());
                    Drawable ebg = getResources().getDrawable(e);
                    txt.setBackground(ebg);

                }
                break;
            case "M":
                if(l.isCompleted() == true){
                    String mbg = "yellowlightbg";
                    int medium = getResources().getIdentifier(mbg, "drawable", getPackageName());
                    Drawable medbg = getResources().getDrawable(medium);
                    txt.setBackground(medbg);
                }else{
                    String mg = "greylightbg";
                    int m = getResources().getIdentifier(mg, "drawable", getPackageName());
                    Drawable mbg = getResources().getDrawable(m);
                    txt.setBackground(mbg);

                }
                break;
            case "H":
                if(l.isCompleted() == true){
                    String hbg = "redlightbg";
                    int hard = getResources().getIdentifier(hbg, "drawable", getPackageName());
                    Drawable hardbg = getResources().getDrawable(hard);
                    txt.setBackground(hardbg);
                }else{
                    String hg = "greylightbg";
                    int h = getResources().getIdentifier(hg, "drawable", getPackageName());
                    Drawable hbg = getResources().getDrawable(h);
                    txt.setBackground(hbg);

                }
                break;
        }
    }


//    public void loadProgress(){
//        checkProgress("E");
//        checkProgress("M");
//        checkProgress("H");
//
//    }

    // set lessons object to completed for those saved in DB from latest activity
    private void checkProgress(String lessonLevel){
        Cursor cursor = loginDB.lessonCompleted(accountState.getActiveUser().getUsername(),lessonLevel);
        if (cursor == null){
            System.out.println("no lesson is completed");
        }
        else{
            int num = cursor.getCount();
            if(cursor.moveToNext()){
                for (int i = 0; i <= num; i++) {
                    @SuppressLint("Range") String level = cursor.getString(cursor.getColumnIndex("level"));
                    @SuppressLint("Range") String id = cursor.getString(cursor.getColumnIndex("id"));
                    if (i + 1 == Integer.parseInt(id) && level.equals(lessonLevel)) {
                        switch (lessonLevel){
                            case "E":
                                loadLesson.getLessonEasyList().get(i).setCompleted(true);
                                break;
                            case "M":
                                loadLesson.getLessonMediumList().get(i).setCompleted(true);
                                break;
                            case "H":
                                loadLesson.getLessonHardList().get(i).setCompleted(true);
                                break;
                        }
                    }
                }
            }
        }
    }

    public void checkCompleted(String username) {
        loginDB.checkLesson(username);
        cursor = loginDB.getLessonProgress();

        if (loginDB.getLessonProgress() == null) {
            System.out.println("Haven't started lesson");
        } else {
            int num = cursor.getCount();

            if (cursor.moveToFirst()) {
                for (int i = 1; i <= num; i++) {
                    @SuppressLint("Range") String level = cursor.getString(cursor.getColumnIndex("level"));
                    @SuppressLint("Range") String id = cursor.getString(cursor.getColumnIndex("id"));
                    switch (level) {
                        case "E":
                            for (int k = 0; k < loadLesson.getLessonEasyList().size(); k++) {
                                System.out.println("easy lesson size : " + loadLesson.getLessonEasyList().size());
                                System.out.println("index is : " + k);
                                if (k + 1 == Integer.parseInt(id)) {
                                    loadLesson.getLessonEasyList().get(k).setCompleted(true);
                                }
                            }
                            break;
                        case "M":
                            for (int k = 0; k < loadLesson.getLessonMediumList().size(); k++) {
                                if (k + 1 == Integer.parseInt(id)) {
                                    loadLesson.getLessonMediumList().get(k).setCompleted(true);
                                }
                            }
                            break;
                        case "H":
                            for (int k = 0; k < loadLesson.getLessonHardList().size(); k++) {
                                if (k + 1 == Integer.parseInt(id)) {
                                    loadLesson.getLessonHardList().get(k).setCompleted(true);
                                }
                            }
                            break;
                    }
                    cursor.moveToNext();
                }
            }
        }
    }




    private boolean levelDone(String level){
        boolean isDone = false;
        Cursor cursor = loginDB.lessonCompleted(accountState.getActiveUser().getUsername(),level);
        if (cursor == null){
            System.out.println("not started in easy level yet!");
            isDone = false;
        }
        else{
            int num = cursor.getCount();
            switch (level) {
                case "E":
                    if (num == loadLesson.getLessonEasyList().size()) {
                        isDone = true;
                    }
                    break;
                case "M":
                    if (num == loadLesson.getLessonMediumList().size()) {
                        isDone = true;
                    }
                    break;
                case "H":
                    if (num == loadLesson.getLessonHardList().size()) {
                        isDone = true;
                    }
                    break;
            }
        }
        return isDone;
    }


    public void onClick(View v) {

        Intent intent;

        switch (v.getId()) {

            //mean button bar
            case R.id.btnLessonL:
                intent = new Intent(LessonListActivity.this,LessonEnterActivity.class);
                startActivity(intent);
                break;

            case R.id.btnLessonQ:
                intent = new Intent(LessonListActivity.this,QuizListActivity.class);
                startActivity(intent);
                break;

            case R.id.btnLessonA:
                intent = new Intent(LessonListActivity.this,AccountActivity.class);
                startActivity(intent);
                break;

            case R.id.btnLessonO:
                intent = new Intent(LessonListActivity.this,MainActivity.class);
                startActivity(intent);
                break;



        // each lesson button
            case R.id.txtE1:
                loadLesson.setLevel(1);
                loadLesson.setActiveLessonIndex(0);
                intent = new Intent(LessonListActivity.this, LessonContentActivity.class);
                startActivity(intent);
                break;

            case R.id.txtE2:
                loadLesson.setLevel(1);
                loadLesson.setActiveLessonIndex(1);
                intent = new Intent(LessonListActivity.this, LessonContentActivity.class);
                startActivity(intent);
                break;


            case R.id.txtE3:
                loadLesson.setLevel(1);
                loadLesson.setActiveLessonIndex(2);
                intent = new Intent(LessonListActivity.this, LessonContentActivity.class);
                startActivity(intent);
                break;

            case R.id.txtE4:
                loadLesson.setLevel(1);
                loadLesson.setActiveLessonIndex(3);
                intent = new Intent(LessonListActivity.this, LessonContentActivity.class);
                startActivity(intent);
                break;





            case R.id.txtM1:
                if (levelDone("E") == true) {
                    loadLesson.setLevel(2);
                    loadLesson.setActiveLessonIndex(0);
                    intent = new Intent(LessonListActivity.this, LessonContentActivity.class);
                    startActivity(intent);
                } else {
                    intent = new Intent(LessonListActivity.this, PopNotAllow.class );
                    startActivity(intent);
                }
                break;

            case R.id.txtM2:
                if (levelDone("E") == true) {
                    loadLesson.setLevel(2);
                    loadLesson.setActiveLessonIndex(1);
                    intent = new Intent(LessonListActivity.this, LessonContentActivity.class);
                    startActivity(intent);
                } else {
                    intent = new Intent(LessonListActivity.this, PopNotAllow.class );
                    startActivity(intent);
                }
                break;



            case R.id.txtM3:
                if (levelDone("E") == true) {
                    loadLesson.setLevel(2);
                    loadLesson.setActiveLessonIndex(2);
                    intent = new Intent(LessonListActivity.this, LessonContentActivity.class);
                    startActivity(intent);
                } else {
                    intent = new Intent(LessonListActivity.this, PopNotAllow.class );
                    startActivity(intent);
                }
                break;

            case R.id.txtM4:
                if (levelDone("E") == true) {
                    loadLesson.setLevel(2);
                    loadLesson.setActiveLessonIndex(3);
                    intent = new Intent(LessonListActivity.this, LessonContentActivity.class);
                    startActivity(intent);
                } else {
                    intent = new Intent(LessonListActivity.this, PopNotAllow.class );
                    startActivity(intent);
                }
                break;


            case R.id.txtH1:
                if (levelDone("E") == true && levelDone("M") == true) {
                    loadLesson.setLevel(3);
                    loadLesson.setActiveLessonIndex(0);
                    intent = new Intent(LessonListActivity.this, LessonContentActivity.class);
                    startActivity(intent);
                } else {
                    intent = new Intent(LessonListActivity.this, PopNotAllow.class );
                    startActivity(intent);
                }
                break;

            case R.id.txtH2:
                if (levelDone("E") == true && levelDone("M") == true) {
                    loadLesson.setLevel(3);
                    loadLesson.setActiveLessonIndex(1);
                    intent = new Intent(LessonListActivity.this, LessonContentActivity.class);
                    startActivity(intent);
                } else {
                    intent = new Intent(LessonListActivity.this, PopNotAllow.class );
                    startActivity(intent);
                }
                break;

            case R.id.txtH3:
                if (levelDone("E") == true && levelDone("M") == true) {
                    loadLesson.setLevel(3);
                    loadLesson.setActiveLessonIndex(2);
                    intent = new Intent(LessonListActivity.this, LessonContentActivity.class);
                    startActivity(intent);
                } else {
                    intent = new Intent(LessonListActivity.this, PopNotAllow.class );
                    startActivity(intent);
                }
                break;

            case R.id.txtH4:
                if (levelDone("E") == true && levelDone("M") == true) {
                    loadLesson.setLevel(3);
                    loadLesson.setActiveLessonIndex(3);
                    intent = new Intent(LessonListActivity.this, LessonContentActivity.class);
                    startActivity(intent);
                } else {
                    intent = new Intent(LessonListActivity.this, PopNotAllow.class );
                    startActivity(intent);
                }
                break;

        }
    }
}