package com.example.measuremandarin;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;



public class AccountActivity extends AppCompatActivity {
    private AccountState accountState;
    private LoadQuiz loadQuiz;
    LoginDatabase loginDB;

    TextView txtGreeting;
    TextView txtNamePassword;
    TextView txtQuizResultTitle;
    TextView txtResult1;
    TextView txtResult2;
    TextView txtResult3;
    TextView txtResult4;
    TextView txtResult5;

    Button btnAccountL;
    Button btnAccountQ;
    Button btnAccountA;
    Button btnAccountO;
    Cursor cursor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        accountState = AccountState.getInstance(this);
        loadQuiz = LoadQuiz.getInstance(this);
        loginDB = LoginDatabase.getInstance(this);

        txtGreeting = findViewById(R.id.txtGreeting);
        txtNamePassword = findViewById(R.id.txtNamePassword);
        txtQuizResultTitle = findViewById(R.id.txtQuizResultTitle);
        txtResult1 = findViewById(R.id.txtResult1);
        txtResult2 = findViewById(R.id.txtResult2);
        txtResult3 = findViewById(R.id.txtResult3);
        txtResult4 = findViewById(R.id.txtResult4);
        txtResult5 = findViewById(R.id.txtResult5);
        btnAccountL = findViewById(R.id.btnAccountL);
        btnAccountQ = findViewById(R.id.btnAccountQ);
        btnAccountA = findViewById(R.id.btnAccountA);
        btnAccountO = findViewById(R.id.btnAccountO);

        txtGreeting.setText("Welcome " + accountState.getActiveUser().getUsername());

        txtNamePassword.setText("Username : " + accountState.getActiveUser().getUsername()
                                + "\n Password : " + accountState.getActiveUser().getPassword());
        System.out.println("************");

        loginDB.resultHistory(accountState.getActiveUser().getUsername());
        if (loginDB.getSelectData() == null){
            System.out.println("================");
            txtResult5.setText( "no info");
            txtResult4.setText( "no info");
            txtResult3.setText( "no info");
            txtResult2.setText( "no info");
            txtResult1.setText( "no info");

        }else{
            cursor = loginDB.getSelectData();
            int num = cursor.getCount();
            if (cursor.moveToFirst()) {
                for (int i = 1; i <= num; i++) {
                    @SuppressLint("Range") String date = cursor.getString(cursor.getColumnIndex("date"));
                    @SuppressLint("Range") String username = cursor.getString(cursor.getColumnIndex("username"));
                    @SuppressLint("Range") String level = cursor.getString(cursor.getColumnIndex("level"));
                    @SuppressLint("Range") String result = cursor.getString(cursor.getColumnIndex("quizresult"));
                    switch (i) {
                        case 1:
                            txtResult1.setText(date + "  Level :"+level+"  Score : " + result);
                            break;
                        case 2:
                            txtResult2.setText(date + "  Level :"+level+"  Score : " + result);
                            break;
                        case 3:
                            txtResult3.setText(date + "  Level :"+level+"  Score : " + result);
                            break;
                        case 4:
                            txtResult4.setText(date + "  Level :"+level+"  Score : " + result);
                            break;
                        case 5:
                            txtResult5.setText(date + "  Level :"+level+"  Score : " + result);
                            break;
                    }
                    cursor.moveToNext();
                }
            }
        }

    }


    public void onClick(View view){
        Intent intent;
        switch(view.getId()){

            //menu button bar
            case R.id.btnAccountL:
                intent = new Intent(AccountActivity.this,LessonEnterActivity.class);
                startActivity(intent);
                break;

            case R.id.btnAccountQ:
                intent = new Intent(AccountActivity.this,QuizListActivity.class);
                startActivity(intent);
                break;

            case R.id.btnAccountA:
                intent = new Intent(AccountActivity.this,AccountActivity.class);
                startActivity(intent);
                break;

            case R.id.btnAccountO:
                intent = new Intent(AccountActivity.this,MainActivity.class);
                startActivity(intent);
                break;
        }
    }

}