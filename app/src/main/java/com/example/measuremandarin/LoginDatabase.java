package com.example.measuremandarin;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import androidx.annotation.Nullable;

public class LoginDatabase extends SQLiteOpenHelper {

    private static LoginDatabase instance;
    private static final String dbName = "AccountData";
    Cursor selectData;
    Cursor lessonProgress;
    Cursor medalEarned;
    Context context;

    public static LoginDatabase getInstance(Context context){
        if(instance == null) {
            instance = new LoginDatabase(context);
        }
        return instance;
    }

    private LoginDatabase(Context context) {
        super(context, "AccountRecord", null, 1);
    }  //change name if revised table

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table users(username TEXT primary key,password TEXT)");
        db.execSQL("create table quizRecord(date TEXT primary key,username TEXT, quizresult TEXT, level TEXT, medal TEXT)");
        db.execSQL("create table lessonRecord(level TEXT ,id TEXT , username TEXT)");
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("drop table if exists users");
        db.execSQL("drop table if exists quizRecord");
        db.execSQL("drop table if exists lessonRecord");
        onCreate(db);
    }

    public boolean insertData(String username, String password){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("username",username);
        values.put("password",password);

        long resultInsert = db.insert("users",null, values);
        if(resultInsert == -1) return false;
        else
            return true;

    }

    public void saveLessonProgress(String level, String id, String username){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("level",level);
        values.put("id",id);
        values.put("username",username);
        db.insert("lessonRecord",null, values);
        setLessonProgress(lessonCompleted(username,level));
        System.out.println("Saved : Lesson: " + level + " id : "+ id);
    }


    public void removeLessonProgress(String level, String id, String username){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("lessonRecord"," level=? and id=? and username=?",new String[]{level,id,username});
        setLessonProgress(lessonCompleted(username,level));
        System.out.println("Not completed : Lesson: " + level + " id : "+ id);
    }

    public void checkLesson(String username){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from lessonRecord where username=? ", new String[]{username});
        if (cursor.getCount() != 0) {
            this.setLessonProgress(cursor);
            System.out.println("lesson save to completed : " +cursor.getCount());
        }
    }

        public Cursor lessonCompleted(String username, String level){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from lessonRecord where username = ? and level =? ", new String[]{username, level});
        if (cursor.getCount() != 0) {
            System.out.println(level + "lesson progress checked!");
        }
        return cursor;
    }



//    public Cursor easyCompleted(String username, String level){
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery("select * from lessonRecord where username = ? and level =? ", new String[]{username, level});
//        if (cursor.getCount() != 0) {
//            System.out.println("easy lesson progress checked!");
//        }
//        return cursor;
//    }
//
//    public Cursor medCompleted(String username, String level){
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery("select * from lessonRecord where username = ? and level =? ", new String[]{username, level});
//        if (cursor.getCount() != 0) {
//            System.out.println("med lesson progress checked!");
//        }
//        return cursor;
//    }
//
//    public Cursor hardCompleted(String username, String level){
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery("select * from lessonRecord where username = ? and level =? ", new String[]{username, level});
//        if (cursor.getCount() != 0) {
//            System.out.println("hard lesson progress checked!");
//        }
//        return cursor;
//    }



    public void saveResult(String date, String username, String result,String level, String medal){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("date",date);
        values.put("username",username);
        values.put("quizresult",result);
        values.put("level", level);
        values.put("medal", medal);
        db.insert("quizRecord",null, values);
    }

    public void resultHistory(String username) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from quizRecord where username=? order by date DESC LIMIT 5", new String[]{username});
        System.out.println("!!!!");
        if (cursor.getCount() != 0) {
            this.setSelectData(cursor);
        }
    }

    public boolean earnMedal(String username, String medal){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from quizRecord where username=? and medal=? ", new String[]{username,medal});
        if (cursor.getCount()>0){
            this.setMedalEarned(cursor);
            return true;
        }
        else{
            System.out.println("nothing found");
            return false;
        }
    }




    public boolean checkUsername(String username){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from users where username=?", new String[] {username});
        if (cursor.getCount()>0){
            return true;
        }else{
            return false;
        }
    }

    public boolean checkUsernamePassword(String username, String password){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from users where username=? and password=?", new String[] {username,password});
        if (cursor.getCount()>0){
            return true;
        }else{
            return false;
        }
    }

    public Cursor getSelectData() {
        return selectData;
    }

    public void setSelectData(Cursor selectData) {
        this.selectData = selectData;
    }

    public Cursor getMedalEarned() {
        return medalEarned;
    }

    public void setMedalEarned(Cursor metalEarned) {
        this.medalEarned = metalEarned;
    }

    public Cursor getLessonProgress() {
        return lessonProgress;
    }

    public void setLessonProgress(Cursor lessonProgress) {
        this.lessonProgress = lessonProgress;
    }
}
