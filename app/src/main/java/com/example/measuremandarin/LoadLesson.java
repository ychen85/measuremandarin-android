package com.example.measuremandarin;

import android.content.Context;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class LoadLesson {

    private static LoadLesson instance;
    private ArrayList<Lesson> lessonEasyList;
    private ArrayList<Lesson> lessonMediumList;
    private ArrayList<Lesson> lessonHardList;

    private Lesson activeLesson;
    private static int activeLessonIndex;
    private static int level; // 1 = easy , 2 = medium , 3 = hard


    public static LoadLesson getInstance(Context context){
        if(instance == null){
            instance = new LoadLesson(context);
        }
        return instance;
    }

    private LoadLesson(Context context) {
        // load json into string
        InputStream is = context.getResources().openRawResource(R.raw.lessons);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String jsonString = writer.toString();




        activeLessonIndex = 0;


        this.lessonEasyList = new ArrayList<>();
        this.lessonMediumList = new ArrayList<>();
        this.lessonHardList = new ArrayList<>();

        JSONParser parser = new JSONParser();
        try {
            //Object object = parser.parse(new FileReader("raw/users.json"));
            Object object = parser.parse(jsonString);

            //convert Object to JSONObject
            org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) object;

            //Reading the array
            org.json.simple.JSONArray easyJSONArray = (org.json.simple.JSONArray) jsonObject.get("lessonsEasy");
            org.json.simple.JSONObject easyJSONObject;

            //Create the objects
            for (Object lesson : easyJSONArray) {
                easyJSONObject = (org.json.simple.JSONObject) lesson;
                lessonEasyList.add(new Lesson(
                        ((org.json.simple.JSONObject) lesson).get("lessonID").toString(),
                        ((org.json.simple.JSONObject) lesson).get("level").toString(),
                        ((org.json.simple.JSONObject) lesson).get("nameCN").toString(),
                        ((org.json.simple.JSONObject) lesson).get("namePN").toString(),
                        ((org.json.simple.JSONObject) lesson).get("use").toString(),
                        ((org.json.simple.JSONObject) lesson).get("exampleCN1").toString(),
                        ((org.json.simple.JSONObject) lesson).get("examplePN1").toString(),
                        ((org.json.simple.JSONObject) lesson).get("exampleEN1").toString(),
                        ((org.json.simple.JSONObject) lesson).get("exampleCN2").toString(),
                        ((org.json.simple.JSONObject) lesson).get("examplePN2").toString(),
                        ((org.json.simple.JSONObject) lesson).get("exampleEN1").toString(),
                        ((org.json.simple.JSONObject) lesson).get("img1").toString(),
                        ((org.json.simple.JSONObject) lesson).get("img2").toString(),
                        ((org.json.simple.JSONObject) lesson).get("audio").toString()));
            }



            //Reading the array
            org.json.simple.JSONArray mediumJSONArray = (org.json.simple.JSONArray) jsonObject.get("lessonsMedium");
            org.json.simple.JSONObject mediumJSONObject;

            //Create the objects
            for (Object lesson : mediumJSONArray) {
                mediumJSONObject = (org.json.simple.JSONObject) lesson;
                lessonMediumList.add(new Lesson(
                        ((org.json.simple.JSONObject) lesson).get("lessonID").toString(),
                        ((org.json.simple.JSONObject) lesson).get("level").toString(),
                        ((org.json.simple.JSONObject) lesson).get("nameCN").toString(),
                        ((org.json.simple.JSONObject) lesson).get("namePN").toString(),
                        ((org.json.simple.JSONObject) lesson).get("use").toString(),
                        ((org.json.simple.JSONObject) lesson).get("exampleCN1").toString(),
                        ((org.json.simple.JSONObject) lesson).get("examplePN1").toString(),
                        ((org.json.simple.JSONObject) lesson).get("exampleEN1").toString(),
                        ((org.json.simple.JSONObject) lesson).get("exampleCN2").toString(),
                        ((org.json.simple.JSONObject) lesson).get("examplePN2").toString(),
                        ((org.json.simple.JSONObject) lesson).get("exampleEN1").toString(),
                        ((org.json.simple.JSONObject) lesson).get("img1").toString(),
                        ((org.json.simple.JSONObject) lesson).get("img2").toString(),
                        ((org.json.simple.JSONObject) lesson).get("audio").toString()));
            }

            //Reading the array
            org.json.simple.JSONArray hardJSONArray = (org.json.simple.JSONArray) jsonObject.get("lessonsHard");
            org.json.simple.JSONObject hardJSONObject;

            //Create the objects
            for (Object lesson : hardJSONArray) {
                hardJSONObject = (org.json.simple.JSONObject) lesson;
                lessonHardList.add(new Lesson(
                        ((org.json.simple.JSONObject) lesson).get("lessonID").toString(),
                        ((org.json.simple.JSONObject) lesson).get("level").toString(),
                        ((org.json.simple.JSONObject) lesson).get("nameCN").toString(),
                        ((org.json.simple.JSONObject) lesson).get("namePN").toString(),
                        ((org.json.simple.JSONObject) lesson).get("use").toString(),
                        ((org.json.simple.JSONObject) lesson).get("exampleCN1").toString(),
                        ((org.json.simple.JSONObject) lesson).get("examplePN1").toString(),
                        ((org.json.simple.JSONObject) lesson).get("exampleEN1").toString(),
                        ((org.json.simple.JSONObject) lesson).get("exampleCN2").toString(),
                        ((org.json.simple.JSONObject) lesson).get("examplePN2").toString(),
                        ((org.json.simple.JSONObject) lesson).get("exampleEN1").toString(),
                        ((org.json.simple.JSONObject) lesson).get("img1").toString(),
                        ((org.json.simple.JSONObject) lesson).get("img2").toString(),
                        ((org.json.simple.JSONObject) lesson).get("audio").toString()));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public ArrayList<Lesson> getLessonEasyList() {
        return lessonEasyList;
    }

    public ArrayList<Lesson> getLessonMediumList() {
        return lessonMediumList;
    }

    public ArrayList<Lesson> getLessonHardList() {
        return lessonHardList;
    }

    public static int getActiveLessonIndex() {
        return activeLessonIndex;
    }

    public static void setActiveLessonIndex(int activeLessonIndex) {
        LoadLesson.activeLessonIndex = activeLessonIndex;
    }

    public static int getLevel() {
        return level;
    }

    public static void setLevel(int level) {
        LoadLesson.level = level;
    }

    public Lesson getActiveLesson() {
        return activeLesson;
    }

    public void setActiveLesson(Lesson activeLesson) {
        this.activeLesson = activeLesson;
    }
}
