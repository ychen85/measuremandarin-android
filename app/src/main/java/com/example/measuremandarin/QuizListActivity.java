package com.example.measuremandarin;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class QuizListActivity extends AppCompatActivity {
    private AccountState accountState;
    private LoginDatabase loginDB;
    private LoadQuiz loadQuiz;
    private TextView txtEasy;
    private TextView txtMedium;
    private TextView txtHard;
    private Button btnQuizEasy;
    private Button btnQuizMedium;
    private Button btnQuizHard;
    private Button btnQuizL;
    private Button btnQuizQ;
    private Button btnQuizA;
    private Button btnQuizO;
    private ImageView easyMedal;
    private ImageView medMedal;
    private ImageView hardMedal;
    private CheckBox btnDisplayMode;
    private boolean isNoInput = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_list);
        accountState = AccountState.getInstance(this);
        loadQuiz = LoadQuiz.getInstance(this);
        loginDB = LoginDatabase.getInstance(this);
        txtEasy = findViewById(R.id.txtEasy);
        txtMedium = findViewById(R.id.txtMedium);
        txtHard = findViewById(R.id.txtHard);

        btnQuizEasy = findViewById(R.id.btnQuizEasy);
        btnQuizMedium = findViewById(R.id.btnQuizMedium);
        btnQuizHard = findViewById(R.id.btnQuizHard);

        btnQuizL = findViewById(R.id.btnQuizL);
        btnQuizQ = findViewById(R.id.btnQuizQ);
        btnQuizA = findViewById(R.id.btnQuizA);
        btnQuizO = findViewById(R.id.btnQuizO);

        easyMedal = findViewById(R.id.easyMedal);
        medMedal = findViewById(R.id.medMedal);
        hardMedal = findViewById(R.id.hardMedal);
        btnDisplayMode = findViewById(R.id.btnDisplayMode);

        //checkbox display
        if(this.isNoInput == true){
            btnDisplayMode.setChecked(true);
            System.out.println("check checkbox status");
        } else {
            btnDisplayMode.setChecked(false);
        }


        //should be read medal info from database
        if (loginDB.earnMedal(accountState.getActiveUser().getUsername(), "1") == true) {
            Cursor cursor = loginDB.getMedalEarned();
            int num = cursor.getCount();
            if (num > 0) {
                if (cursor.moveToFirst()) {
                    for (int i = 1; i <= num; i++) {
                        @SuppressLint("Range") String level = cursor.getString(cursor.getColumnIndex("level"));

                        switch (level) {
                            case "Easy":
                                accountState.setEasyDone(true);
                                String easyM = "medal_on";
                                int easy = getResources().getIdentifier(easyM, "drawable", getPackageName());
                                Drawable drawableEasyM = getResources().getDrawable(easy);
                                easyMedal.setBackground(drawableEasyM);
                                break;
                            case "Medium":
                                accountState.setMedDone(true);
                                String medM = "medal_on";
                                int med = getResources().getIdentifier(medM, "drawable", getPackageName());
                                Drawable drawableMedM = getResources().getDrawable(med);
                                medMedal.setBackground(drawableMedM);
                                break;

                            case "Hard":
                                accountState.setHardDone(true);
                                String hardM = "medal_on";
                                int hard = getResources().getIdentifier(hardM, "drawable", getPackageName());
                                Drawable drawableHardM = getResources().getDrawable(hard);
                                hardMedal.setBackground(drawableHardM);
                                break;
                        }
                        cursor.moveToNext();
                    }
                }

            }else{
                accountState.setEasyDone(false);
                accountState.setMedDone(false);
                accountState.setHardDone(false);
                System.out.println("no medal yet");
            }
        }
    }


    public void onClick(View view) {

        Intent intent;

        switch (view.getId()) {

            //menu button bar
            case R.id.btnQuizL:
                intent = new Intent(QuizListActivity.this,LessonEnterActivity.class);
                startActivity(intent);
                break;

            case R.id.btnQuizQ:
                intent = new Intent(QuizListActivity.this,QuizListActivity.class);
                startActivity(intent);
                break;

            case R.id.btnQuizA:
                intent = new Intent(QuizListActivity.this,AccountActivity.class);
                startActivity(intent);
                break;

            case R.id.btnQuizO:
                intent = new Intent(QuizListActivity.this,MainActivity.class);
                startActivity(intent);
                break;


            case R.id.txtEasy:
            case R.id.btnQuizEasy:
                loadQuiz.setLevel(1);
                loadQuiz.setActiveQuizIndex(0);
                if (btnDisplayMode.isChecked() == true && isNoInput == true){

                    intent = new Intent(QuizListActivity.this, QuizNoInputActivity.class);
                    startActivity(intent);
                    break;
                } else {
                    loadQuiz.setCurPoint(0);
                    intent = new Intent(QuizListActivity.this, QuizContentActivity.class);
                    startActivity(intent);
                }
                break;


            case R.id.txtMedium:
            case R.id.btnQuizMedium:
                loadQuiz.setLevel(2);
                loadQuiz.setActiveQuizIndex(0);
                if (btnDisplayMode.isChecked() == true && isNoInput == true){
                    intent = new Intent(QuizListActivity.this, QuizNoInputActivity.class);
                    startActivity(intent);
                    break;
                } else {

                    loadQuiz.setCurPoint(0);
                    intent = new Intent(QuizListActivity.this, QuizContentActivity.class);
                    startActivity(intent);
                }
                break;

            case R.id.txtHard:
            case R.id.btnQuizHard:
                loadQuiz.setLevel(3);
                loadQuiz.setActiveQuizIndex(0);
                if (btnDisplayMode.isChecked() == true && isNoInput == true){
                    intent = new Intent(QuizListActivity.this, QuizNoInputActivity.class);
                    startActivity(intent);
                    break;
                } else {

                    loadQuiz.setCurPoint(0);
                    intent = new Intent(QuizListActivity.this, QuizContentActivity.class);
                    startActivity(intent);
                }
                break;

            case R.id.btnDisplayMode:

                if(btnDisplayMode.isChecked() == true){
                    isNoInput = true;
                    System.out.println("Display Mode ON");
                }else {
                    isNoInput = false;
                    System.out.println("Display Mode Off");
                }

                break;
            }
    }

}