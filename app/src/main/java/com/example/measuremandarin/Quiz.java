package com.example.measuremandarin;

public class Quiz {

    private String qid;
    private String level;
    private String testWordCN;
    private String target;
    private String type; // "mix", "one"
    private String imgInfo;
    private String question;
    private String answer;
    private String optionCN1;
    private String optionPN1;
    private String optionCN2;
    private String optionPN2;
    private String optionCN3;
    private String optionPN3;
    private String optionCN4;
    private String optionPN4;
    private String src;


    public Quiz(String qid, String level, String testWordCN, String target,
                String type, String imgInfo, String question, String answer,
                String optionCN1, String optionPN1,
                String optionCN2, String optionPN2,
                String optionCN3, String optionPN3,
                String optionCN4, String optionPN4, String src) {
        this.qid = qid;
        this.level = level;
        this.testWordCN = testWordCN;
        this.target = target;
        this.type = type;
        this.imgInfo = imgInfo;
        this.question = question;
        this.answer = answer;
        this.optionCN1 = optionCN1;
        this.optionPN1 = optionPN1;
        this.optionCN2 = optionCN2;
        this.optionPN2 = optionPN2;
        this.optionCN3 = optionCN3;
        this.optionPN3 = optionPN3;
        this.optionCN4 = optionCN4;
        this.optionPN4 = optionPN4;
        this.src = src;
    }

    public String getTestWordCN() {
        return testWordCN;
    }

    public void setTestWordCN(String testWordCN) {
        this.testWordCN = testWordCN;
    }

    public String getGetTestWordPN() {
        return target;
    }

    public void setGetTestWordPN(String target) {
        this.target = target;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getImgInfo() {
        return imgInfo;
    }

    public void setImgInfo(String imgInfo) {
        this.imgInfo = imgInfo;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getOptionCN1() {
        return optionCN1;
    }

    public void setOptionCN1(String optionCN1) {
        this.optionCN1 = optionCN1;
    }

    public String getOptionPN1() {
        return optionPN1;
    }

    public void setOptionPN1(String optionPN1) {
        this.optionPN1 = optionPN1;
    }

    public String getOptionCN2() {
        return optionCN2;
    }

    public void setOptionCN2(String optionCN2) {
        this.optionCN2 = optionCN2;
    }

    public String getOptionPN2() {
        return optionPN2;
    }

    public void setOptionPN2(String optionPN2) {
        this.optionPN2 = optionPN2;
    }

    public String getOptionCN3() {
        return optionCN3;
    }

    public void setOptionCN3(String optionCN3) {
        this.optionCN3 = optionCN3;
    }

    public String getOptionPN3() {
        return optionPN3;
    }

    public void setOptionPN3(String optionPN3) {
        this.optionPN3 = optionPN3;
    }

    public String getOptionCN4() {
        return optionCN4;
    }

    public void setOptionCN4(String optionCN4) {
        this.optionCN4 = optionCN4;
    }

    public String getOptionPN4() {
        return optionPN4;
    }

    public void setOptionPN4(String optionPN4) {
        this.optionPN4 = optionPN4;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getQid() {
        return qid;
    }

    public void setQid(String qid) {
        this.qid = qid;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}
