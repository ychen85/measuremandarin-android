package com.example.measuremandarin;

import java.util.ArrayList;

public class User {

    String username;
    String password;
    ArrayList<Quiz> taken;
    int result;
    String date;

    public User(String username, String password){
        this.username = username;
        this.password = password;
        this.taken = new ArrayList<>();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }






    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
