package com.example.measuremandarin;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;

public class PopTrue extends AppCompatActivity {

    Button btnCloseTrue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_true);


        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int popW = dm.widthPixels;
        int popH = dm.heightPixels;
        getWindow().setLayout((int)(popW*.8), (int)(popH*.6));

        btnCloseTrue = findViewById(R.id.btnCloseTrue);

    }


    public void onClick(View view){
        finish();
    }
}