package com.example.measuremandarin;

import static java.util.Collections.shuffle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;
import java.util.stream.Collectors;

public class QuizContentActivity extends AppCompatActivity {

    TextView txtOption1;
    TextView txtOption2;
    TextView txtOption3;
    TextView txtOption4;
    TextView correct;
    TextView txtQuestion1;
    TextView txtQuestion2;
    ImageView btnEnd;
    ImageView btnRestart;
    ImageView imgQuiz;
    ImageView imgLevel;

    String optionCN1;
    String optionPN1;
    String optionEN1;
    String optionCN2;
    String optionPN2;
    String optionEN2;
    String optionCN3;
    String optionPN3;
    String optionEN3;
    String optionCN4;
    String optionPN4;
    String optionEN4;

    int level;  // 1:easy 2: medium 3: hard 4:failed quiz
    private LoadQuiz loadQuiz;
    private ArrayList<Quiz> process;

    ArrayList<Quiz> displayQuiz;
    RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_content);
        relativeLayout = findViewById(R.id.quizContentLayout);
        loadQuiz = LoadQuiz.getInstance(this);
        level = loadQuiz.getLevel();

        txtQuestion1 = findViewById(R.id.txtQuestion1);
        txtQuestion2 = findViewById(R.id.txtQuestion2);
        txtOption1 = findViewById(R.id.txtOption1);
        txtOption2 = findViewById(R.id.txtOption2);
        txtOption3 = findViewById(R.id.txtOption3);
        txtOption4 = findViewById(R.id.txtOption4);

        btnEnd = findViewById(R.id.imgEnd);
        btnRestart = findViewById(R.id.imgRestart);
        imgQuiz = findViewById(R.id.imgQuiz);
        imgLevel = findViewById(R.id.imgLevelQuiz);




        switch (level) {
            case 1:
                relativeLayout.setBackgroundResource(R.drawable.quiz_easy_base);
                displayQuiz = genDisplayQuiz(loadQuiz.getQuizEasyList());  //generate questions for this quiz

                loadQuiz.setCurQuizList(displayQuiz);
                loadQuiz.setActiveQuiz(displayQuiz.get(loadQuiz.getActiveQuizIndex()));
                String nameEasyTitle = "easy_headline";
                int idEasyTitle = getResources().getIdentifier(nameEasyTitle, "drawable", getPackageName());
                Drawable drawableIDET = getResources().getDrawable(idEasyTitle);
                imgLevel.setBackground(drawableIDET);

                String nameEasyFrame = "btneasyquizframe";
                int easy = getResources().getIdentifier(nameEasyFrame, "drawable", getPackageName());
                Drawable drawableEasyF = getResources().getDrawable(easy);
                txtOption1.setBackground(drawableEasyF);
                txtOption2.setBackground(drawableEasyF);
                txtOption3.setBackground(drawableEasyF);
                txtOption4.setBackground(drawableEasyF);

                //quiz pic name get from src
                String name1 = loadQuiz.getCurQuizList().get(loadQuiz.getActiveQuizIndex()).getSrc();
                int id1 = getResources().getIdentifier(name1, "drawable", getPackageName());
                Drawable drawable1 = getResources().getDrawable(id1);
                imgQuiz.setBackground(drawable1);
                break;


            case 2:
                relativeLayout.setBackgroundResource(R.drawable.quiz_medium_base);
                displayQuiz = genDisplayQuiz(loadQuiz.getQuizMediumList());
                loadQuiz.setCurQuizList(displayQuiz);
                loadQuiz.setActiveQuiz(displayQuiz.get(loadQuiz.getActiveQuizIndex()));
                String nameMediumTitle = "medium_headline";
                int idMediumTitle = getResources().getIdentifier(nameMediumTitle, "drawable", getPackageName());
                Drawable drawableIDMT = getResources().getDrawable(idMediumTitle);
                imgLevel.setBackground(drawableIDMT);


                String nameMediumFrame = "btnmediumquizframe";
                int medium = getResources().getIdentifier(nameMediumFrame, "drawable", getPackageName());
                Drawable drawableMediumF = getResources().getDrawable(medium);
                txtOption1.setBackground(drawableMediumF);
                txtOption2.setBackground(drawableMediumF);
                txtOption3.setBackground(drawableMediumF);
                txtOption4.setBackground(drawableMediumF);

                String name2 = loadQuiz.getCurQuizList().get(loadQuiz.getActiveQuizIndex()).getSrc();
                int id2 = getResources().getIdentifier(name2, "drawable", getPackageName());
                Drawable drawable2 = getResources().getDrawable(id2);
                imgQuiz.setBackground(drawable2);
                break;


            case 3:
                relativeLayout.setBackgroundResource(R.drawable.quiz_hard_base);

                displayQuiz = genDisplayQuiz(loadQuiz.getQuizHardList());
                loadQuiz.setCurQuizList(displayQuiz);
                loadQuiz.setActiveQuiz(displayQuiz.get(loadQuiz.getActiveQuizIndex()));
                String nameHardTitle = "hard_headline";
                int idHardTitle = getResources().getIdentifier(nameHardTitle, "drawable", getPackageName());
                Drawable drawableIDHT = getResources().getDrawable(idHardTitle);
                imgLevel.setBackground(drawableIDHT);


                String nameHardFrame = "btnhardquizframe";
                int hard = getResources().getIdentifier(nameHardFrame, "drawable", getPackageName());
                Drawable drawableHardF = getResources().getDrawable(hard);
                txtOption1.setBackground(drawableHardF);
                txtOption2.setBackground(drawableHardF);
                txtOption3.setBackground(drawableHardF);
                txtOption4.setBackground(drawableHardF);

                String name3 = loadQuiz.getCurQuizList().get(loadQuiz.getActiveQuizIndex()).getSrc();
                int id3 = getResources().getIdentifier(name3, "drawable", getPackageName());
                Drawable drawable3 = getResources().getDrawable(id3);
                imgQuiz.setBackground(drawable3);
                break;

            case 4:
                takeFailedQuiz(loadQuiz.getCurQuizList());
                break;
        }
        System.out.println("current quiz ID : " + loadQuiz.getCurQuizList().get(loadQuiz.getActiveQuizIndex()).getQid());
        optionCN1 = displayQuiz.get(loadQuiz.getActiveQuizIndex()).getOptionCN1();
        optionPN1 = displayQuiz.get(loadQuiz.getActiveQuizIndex()).getOptionPN1();

        optionCN2 = displayQuiz.get(loadQuiz.getActiveQuizIndex()).getOptionCN2();
        optionPN2 = displayQuiz.get(loadQuiz.getActiveQuizIndex()).getOptionPN2();
        optionCN3 = displayQuiz.get(loadQuiz.getActiveQuizIndex()).getOptionCN3();
        optionPN3 = displayQuiz.get(loadQuiz.getActiveQuizIndex()).getOptionPN3();
        optionCN4 = displayQuiz.get(loadQuiz.getActiveQuizIndex()).getOptionCN4();
        optionPN4 = displayQuiz.get(loadQuiz.getActiveQuizIndex()).getOptionPN4();


        txtQuestion1.setText(displayQuiz.get(loadQuiz.getActiveQuizIndex()).getImgInfo());
        txtQuestion2.setText(displayQuiz.get(loadQuiz.getActiveQuizIndex()).getQuestion());

        if (loadQuiz.getActiveQuiz().getClass().equals(MixQuiz.class)) {
            MixQuiz temp = (MixQuiz)loadQuiz.getActiveQuiz();
            optionEN1 = temp.getOptionEN1();
            optionEN2 = temp.getOptionEN2();
            optionEN3 = temp.getOptionEN3();
            optionEN4 = temp.getOptionEN4();

            txtOption1.setText("A : " + optionCN1 + "\n" + optionPN1 + "\n" + optionEN1 );
            txtOption2.setText("B : " + optionCN2 + "\n" + optionPN2 + "\n" + optionEN2 );
            txtOption3.setText("C : " + optionCN3 + "\n" + optionPN3 + "\n" + optionEN3 );
            txtOption4.setText("D : " + optionCN4 + "\n" + optionPN4 + "\n" + optionEN4 );

        } else {
            txtOption1.setText("A : " + optionCN1 + "\n" + optionPN1);
            txtOption2.setText("B : " + optionCN2 + "\n" + optionPN2);
            txtOption3.setText("C : " + optionCN3 + "\n" + optionPN3);
            txtOption4.setText("D : " + optionCN4 + "\n" + optionPN4);

        }
    }
    public void optionOnClick(View view) {
        Intent intent;
        String givenA;
        int answer;
        int response = 0;
//        failed = loadQuiz.getFailed();
        System.out.println("DEBUG: failed quizzes: " + loadQuiz.getFailed().toString());

        switch (view.getId()) {

            case R.id.txtOption1:
                response = 1;
                correct = findViewById(R.id.txtOption1);
                break;
            case R.id.txtOption2:
                response = 2;
                correct = findViewById(R.id.txtOption2);
                break;
            case R.id.txtOption3:
                response = 3;
                correct = findViewById(R.id.txtOption3);
                break;
            case R.id.txtOption4:
                response = 4;
                correct = findViewById(R.id.txtOption4);
                break;
            default:
                break;
        }

        givenA = loadQuiz.getActiveQuiz().getAnswer();
        answer = Integer.parseInt(givenA);
        System.out.println("on new question, current failed : "+ loadQuiz.getFailed().size());
        System.out.println("answering question number " + loadQuiz.getActiveQuizIndex());
        System.out.println("user's answer is : " + response);
        System.out.println("correct answer is : " + loadQuiz.getActiveQuiz().getAnswer());

        //right answer
        if (response == answer) {

            clickAnimation(level,correct);

            // show up correct pop up  window and goback to quiz and refrest activity for next question
            intent = new Intent(QuizContentActivity.this, PopTrue.class );
            System.out.println("Correct message 1");
            startActivity(intent);
            System.out.println("Correct message 2");


            //if this is standard quiz
            if (loadQuiz.isDidFailed() == false) {

                //answer right at first try, situation 1, no failed before
                if (loadQuiz.getFailed().size() == 0) {
                    loadQuiz.setCurPoint(loadQuiz.getCurPoint() + 1);
                    System.out.println("no failed question");

                } else {
                    //if there are failed questions, check if this time is the second or more time try on same question.

                    for (int i = 0; i < loadQuiz.getFailed().size(); i++) {
                        //if it is wrong at the first attempt, no point gained
                        if ((loadQuiz.getActiveQuiz().getQid()).equals(loadQuiz.getFailed().get(i).getQid())) {
                            System.out.println("no points!! already failed");
                            break;
                        }
                        //if current question isn't listed in failed list, first attempt right, get 1 point
                        if (i == loadQuiz.getFailed().size() - 1) {
                            loadQuiz.setCurPoint(loadQuiz.getCurPoint() + 1);
                            System.out.println("right on the first try");
                            break;
                        }
                    }
                }
                //                    loadQuiz.setCurPoint(loadQuiz.getCurPoint() + 1);  //add in to failed list if it is the first wrong answer of this question
                System.out.println("total failed questions : " + loadQuiz.getFailed().size());
                System.out.println("current point is " + loadQuiz.getCurPoint());
                //go to next question
                loadQuiz.setActiveQuizIndex(loadQuiz.getActiveQuizIndex() + 1);
                System.out.println("next question is number : " + loadQuiz.getActiveQuizIndex());
                //when it is the last question to answer

                while (loadQuiz.getActiveQuizIndex() > loadQuiz.getCurQuizList().size() - 1) {



                    //no failed question at all
                    if (loadQuiz.getFailed().size() == 0) {

                        intent = new Intent(QuizContentActivity.this, QuizResultActivity.class);
                        System.out.println("no failed question at all, jump to result");
                        startActivity(intent);
                    } else {

                        //check if current quiz is already a failed collection (didFailed == true)
                        if (loadQuiz.isDidFailed() == true) {
                            intent = new Intent(QuizContentActivity.this, QuizResultActivity.class);
                            System.out.println("failed quesitons reviewed, jump to result");
                            startActivity(intent);

                        } else {
                            //if just finished standard quiz, and have failed some
                            System.out.println("there are failed quizzies!!!");
                            System.out.println("total failed questions : " + loadQuiz.getFailed().size());

                            loadQuiz.setCurQuizList(loadQuiz.getFailed()); //set current quiz to failedl list
                            loadQuiz.setActiveQuizIndex(0);
                            loadQuiz.setLevel(4);// onCreate display setting on failed parts
                            //                       loadQuiz.setFailed(new ArrayList<>());
                            System.out.println("after set current quiz to failed, the size of failed quiz is " + loadQuiz.getFailed().size());
                            intent = getIntent();
                            finish();
                            startActivity(intent);
                            loadQuiz.setDidFailed(true);
                        }

                    }
                    return;
                }



                intent = getIntent();
                finish();
                startActivity(intent);
                this.overridePendingTransition(R.anim.rightin,R.anim.leftout);
            } else {
                // answer right, no point because in review of failed question
                for (int i = 0; i < loadQuiz.getFailed().size(); i++) {
                    //if it is wrong at the first attempt, no point gained
                    if ((loadQuiz.getActiveQuiz().getQid()).equals(loadQuiz.getFailed().get(i).getQid())) {
                        System.out.println("no points!! already failed");
                        loadQuiz.setActiveQuizIndex(loadQuiz.getActiveQuizIndex()+1);

                        while( loadQuiz.getActiveQuizIndex() > loadQuiz.getCurQuizList().size()-1){

                            intent = new Intent(QuizContentActivity.this, QuizResultActivity.class);
                            System.out.println("done all questions, incl. failed ones, jump to result");
                            startActivity(intent);
                            return;
                        }

                        intent = getIntent();
                        finish();
                        startActivity(intent);

                    }
                }
            }
        //if answer wrong answer
        } else {

            intent = new Intent(QuizContentActivity.this, PopFalse.class );
            startActivity(intent);



            Toast.makeText(this, " Wrong Answer! Try again! ", Toast.LENGTH_SHORT).show();
            //if not the right answer
            //check if this question has been answered wrong already.

            if (loadQuiz.getFailed().size() == 0) {
                loadQuiz.getFailed().add(loadQuiz.getActiveQuiz());
//                loadQuiz.setFailed(failed);
                System.out.println("First wrong question, Adding to failed...");
                System.out.println("1 question added. total : " +loadQuiz.getFailed().size());
            } else {
                for (int i = 0; i < loadQuiz.getFailed().size(); i++) {
                    if ((loadQuiz.getFailed().get(i).getQid()).equals(loadQuiz.getActiveQuiz().getQid())) { // for every quiz in Failed, check if it equals active, if it does, break out of loop
                        System.out.println("Wrong question already in failed...");
                        System.out.println(" total : " +loadQuiz.getFailed().size());
                        break;
                    }
                    if (i == loadQuiz.getFailed().size() - 1) { // if at the end of for loop and we still havent broken out, active must not be in failed, therefore add it in
                        loadQuiz.getFailed().add(loadQuiz.getActiveQuiz());
//                        loadQuiz.setFailed(failed);
                        System.out.println("Wrong question not in failed, adding...");
                        System.out.println("1 question added. total : " +loadQuiz.getFailed().size());
                        break;
                    }
                }
            }
        }
    }


    public void onClick(View view){
        Intent intent;

        switch (view.getId()){
            case R.id.imgEnd:
                Toast.makeText(this, "Get your quiz result now!", Toast.LENGTH_SHORT).show();
                intent = new Intent(QuizContentActivity.this, QuizResultActivity.class);
                startActivity(intent);

                break;

            case R.id.imgRestart:
                ArrayList<Quiz> allFailed = new ArrayList<>();
                loadQuiz.setFailed(allFailed);
                System.out.println("empty failed question for new quiz");
                loadQuiz.setCurPoint(0);
                System.out.println("Current point set to 0");
                loadQuiz.setActiveQuizIndex(0);
                System.out.println("display quiz index set to 0");
                intent = new Intent(QuizContentActivity.this, QuizListActivity.class);
                startActivity(intent);
                break;
        }


    }

    private void clickAnimation(int level, TextView correct){

        switch(level){
            case 1:

                String correctFrameE = "greensolid";
                int correctE = getResources().getIdentifier(correctFrameE, "drawable", getPackageName());
                Drawable drawableCE = getResources().getDrawable(correctE);
                correct.setBackground(drawableCE);
                break;
            case 2:

                String correctFrameM = "yellowsolid";
                int correctM = getResources().getIdentifier(correctFrameM, "drawable", getPackageName());
                Drawable drawableCM = getResources().getDrawable(correctM);
                correct.setBackground(drawableCM);
                break;
            case 3:
                String correctFrameH = "redsolid";
                int correctH = getResources().getIdentifier(correctFrameH, "drawable", getPackageName());
                Drawable drawableCH = getResources().getDrawable(correctH);
                correct.setBackground(drawableCH);
                break;
        }

    }



    public void takeFailedQuiz(ArrayList<Quiz> f) {

        switch (loadQuiz.getCurQuizList().get(0).getLevel()) {
            case "Easy":
                relativeLayout.setBackgroundResource(R.drawable.quiz_easy_base);
                displayQuiz = loadQuiz.getCurQuizList();  // display all failed questions
                loadQuiz.setActiveQuiz(loadQuiz.getCurQuizList().get(loadQuiz.getActiveQuizIndex()));


                String nameEasyTitle = "easy_headline";
                int idEasyTitle = getResources().getIdentifier(nameEasyTitle, "drawable", getPackageName());
                Drawable drawableIDET = getResources().getDrawable(idEasyTitle);
                imgLevel.setBackground(drawableIDET);

                String nameEasyFrame = "btneasyquizframe";
                int easy = getResources().getIdentifier(nameEasyFrame, "drawable", getPackageName());
                Drawable drawableEasyF = getResources().getDrawable(easy);
                txtOption1.setBackground(drawableEasyF);
                txtOption2.setBackground(drawableEasyF);
                txtOption3.setBackground(drawableEasyF);
                txtOption4.setBackground(drawableEasyF);

                String name1 = loadQuiz.getCurQuizList().get(loadQuiz.getActiveQuizIndex()).getSrc();
                int id1 = getResources().getIdentifier(name1, "drawable", getPackageName());
                Drawable drawable1 = getResources().getDrawable(id1);
                imgQuiz.setBackground(drawable1);
                break;

            case "Medium":
                relativeLayout.setBackgroundResource(R.drawable.quiz_medium_base);
                displayQuiz = loadQuiz.getCurQuizList();  // display all failed questions
                loadQuiz.setActiveQuiz(loadQuiz.getCurQuizList().get(loadQuiz.getActiveQuizIndex()));


                String nameMediumTitle = "medium_headline";
                int idMediumTitle = getResources().getIdentifier(nameMediumTitle, "drawable", getPackageName());
                Drawable drawableIDMT = getResources().getDrawable(idMediumTitle);
                imgLevel.setBackground(drawableIDMT);


                String nameMediumFrame = "btnmediumquizframe";
                int medium = getResources().getIdentifier(nameMediumFrame, "drawable", getPackageName());
                Drawable drawableMediumF = getResources().getDrawable(medium);
                txtOption1.setBackground(drawableMediumF);
                txtOption2.setBackground(drawableMediumF);
                txtOption3.setBackground(drawableMediumF);
                txtOption4.setBackground(drawableMediumF);

                String name2 = loadQuiz.getCurQuizList().get(loadQuiz.getActiveQuizIndex()).getSrc();
                int id2 = getResources().getIdentifier(name2, "drawable", getPackageName());
                Drawable drawable2 = getResources().getDrawable(id2);
                imgQuiz.setBackground(drawable2);
                break;


            case "Hard":
                relativeLayout.setBackgroundResource(R.drawable.quiz_hard_base);
                displayQuiz = loadQuiz.getCurQuizList();  // display all failed questions
                loadQuiz.setActiveQuiz(loadQuiz.getCurQuizList().get(loadQuiz.getActiveQuizIndex()));


                String nameHardTitle = "hard_headline";
                int idHardTitle = getResources().getIdentifier(nameHardTitle, "drawable", getPackageName());
                Drawable drawableIDHT = getResources().getDrawable(idHardTitle);
                imgLevel.setBackground(drawableIDHT);


                String nameHardFrame = "btnhardquizframe";
                int hard = getResources().getIdentifier(nameHardFrame, "drawable", getPackageName());
                Drawable drawableHardF = getResources().getDrawable(hard);
                txtOption1.setBackground(drawableHardF);
                txtOption2.setBackground(drawableHardF);
                txtOption3.setBackground(drawableHardF);
                txtOption4.setBackground(drawableHardF);

                String name3 = loadQuiz.getCurQuizList().get(loadQuiz.getActiveQuizIndex()).getSrc();
                int id3 = getResources().getIdentifier(name3, "drawable", getPackageName());
                Drawable drawable3 = getResources().getDrawable(id3);
                imgQuiz.setBackground(drawable3);
                break;

            default:
                break;
        }

    }

    //generate a quiz with 2 formats (single/mix) of quesitons for the user
    public ArrayList<Quiz> genDisplayQuiz(ArrayList<Quiz> a){
        ArrayList<Quiz> display = new ArrayList<>();

        switch (loadQuiz.getLevel()){

            case 1:
                //the list hold different questions, aiming at the same target word knowledge(E1.E2..etc)
                ArrayList<Quiz> E1 = new ArrayList<>();
                ArrayList<Quiz> E2 = new ArrayList<>();
                ArrayList<Quiz> E3 = new ArrayList<>();
                ArrayList<Quiz> E4 = new ArrayList<>();

                //get all easy level questions arraylist from JSON
                // group them in different target word
                for (Quiz q : a) {
                    if (q.getTarget().equals("E1")) {
                        E1.add(q);
                    } else if (q.getTarget().equals("E2")) {
                        E2.add(q);
                    } else if (q.getTarget().equals("E3")) {
                        E3.add(q);
                    } else if (q.getTarget().equals("E4")) {
                        E4.add(q);
                    }
                }

                //random select two questions to test 1 target word, in the same level
                Random r1 = new Random();
                int e1 = r1.nextInt(1);
                int e2 = r1.nextInt(3)+1;

                display.add(E1.get(e1));
                display.add(E2.get(e1));
                display.add(E3.get(e1));
                display.add(E4.get(e1));
                display.add(E1.get(e2));
                display.add(E2.get(e2));
                display.add(E3.get(e2));
                display.add(E4.get(e2));

                //add in questions that test a couple of target words in the same question
                display.addAll(loadQuiz.getMixEasy());
                //shuffle the whole list after all questions are selected to be displayed
                //Collections.shuffle(display);
                System.out.println("!!standard quiz size is : " + display.size());
                break;

            case 2:
                ArrayList<Quiz> M1 = new ArrayList<>();
                ArrayList<Quiz> M2 = new ArrayList<>();
                ArrayList<Quiz> M3 = new ArrayList<>();
                ArrayList<Quiz> M4 = new ArrayList<>();

                for (Quiz q : a) {
                    if (q.getTarget().equals("M1")) {
                        M1.add(q);
                    } else if (q.getTarget().equals("M2")) {
                        M2.add(q);
                    } else if (q.getTarget().equals("M3")) {
                        M3.add(q);
                    } else if (q.getTarget().equals("M4")) {
                        M4.add(q);
                    }
                }

                Random r2 = new Random();
                int m1 = r2.nextInt(1);
                int m2 = r2.nextInt(3)+1;

                display.add(M1.get(m1));
                display.add(M2.get(m1));
                display.add(M3.get(m1));
                display.add(M4.get(m1));
                display.add(M1.get(m2));
                display.add(M2.get(m2));
                display.add(M3.get(m2));
                display.add(M4.get(m2));



                //add in questions that test a couple of target words in the same question
                display.addAll(loadQuiz.getMixMedium());
                //shuffle the whole list after all questions are selected to be displayed
                //Collections.shuffle(display);

                System.out.println("standard quiz size is : " + display.size());
                break;

            case 3:
                ArrayList<Quiz> H1 = new ArrayList<>();
                ArrayList<Quiz> H2 = new ArrayList<>();
                ArrayList<Quiz> H3 = new ArrayList<>();
                ArrayList<Quiz> H4 = new ArrayList<>();

                for (Quiz q : a) {
                    if (q.getTarget().equals("H1")) {
                        H1.add(q);
                    } else if (q.getTarget().equals("H2")) {
                        H2.add(q);
                    } else if (q.getTarget().equals("H3")) {
                        H3.add(q);
                    } else if (q.getTarget().equals("H4")) {
                        H4.add(q);
                    }
                }

                Random r3 = new Random();
                int h1 = r3.nextInt(1);
                int h2 = r3.nextInt(3)+1;

                display.add(H1.get(h1));
                display.add(H2.get(h1));
                display.add(H3.get(h1));
                display.add(H4.get(h1));
                display.add(H1.get(h2));
                display.add(H2.get(h2));
                display.add(H3.get(h2));
                display.add(H4.get(h2));

                //add in questions that test a couple of target words in the same question
                display.addAll(loadQuiz.getMixHard());
                //shuffle the whole list after all questions are selected to be displayed
                //Collections.shuffle(display);


                System.out.println("standard quiz size is : " + display.size());
                break;

        }
        return display;
    }

}