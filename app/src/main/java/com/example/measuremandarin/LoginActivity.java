package com.example.measuremandarin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileWriter;

//create sign up page only
public class LoginActivity extends AppCompatActivity {
    private AccountState accountState;
    private EditText edtUsernameLogin;
    private EditText edtPasswordLogin;
    private Button btnLogin;
    LoginDatabase loginDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        accountState = AccountState.getInstance(this);
        edtUsernameLogin = findViewById(R.id.edtUsernameLogin);
        edtPasswordLogin = findViewById(R.id.edtPasswordLogin);
        btnLogin= findViewById(R.id.btnLogin);

        loginDB = LoginDatabase.getInstance(this);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = edtUsernameLogin.getText().toString();
                String password = edtPasswordLogin.getText().toString();


                if(TextUtils.isEmpty(username) || TextUtils.isEmpty(password)){
                    Toast.makeText(LoginActivity.this,"All filed required",Toast.LENGTH_SHORT).show();
                }else{
                    boolean checkMatched = loginDB.checkUsernamePassword(username, password);
                    if(checkMatched == true){

                        accountState.setActiveUser(new User(username,password));
                        Toast.makeText(LoginActivity.this,"Log In Successfully",Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent (LoginActivity.this,AccountActivity.class);
                        startActivity(intent);
                    }else{
                        Toast.makeText(LoginActivity.this,"Username or password is incorrect",Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });


    }




//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_register);
//
//        edtRegUsername = findViewById(R.id.edtRegUsername);
//        edtRegPassword = findViewById(R.id.edtRegPassword);
//        btnRegister = findViewById(R.id.btnRegister);
//
//    }

//    public void onClick(View view) {
//        regUsername = edtRegUsername.getText().toString();
//        regPassword = edtRegPassword.getText().toString();
//
//
//        File file = new File(this.getFilesDir(), "registered");
//        if (!file.exists()) {
//                file.mkdir();
//        }
//        try {
//            File regFile = new File(file, "registered.json");
//            FileWriter writer = new FileWriter(regFile);
//            String regStr = "{ \"userdata\": [{\"username\": \"regUsername\"},{\"password\": \" regPassword \"}] }";
//            writer.append(regStr);
//            writer.flush();
//            writer.close();
//            Toast.makeText(this, "Log in credential stored!", Toast.LENGTH_LONG).show();
//        } catch (Exception e) { }
//
//        user = new User(regUsername, regPassword);
//
//        Intent intent = new Intent(getApplicationContext(), AccountActivity.class);
//        startActivity(intent);
//
//
//
////            settingsState.refreshPIN();
//
//    }
}