package com.example.measuremandarin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;

public class PopFalse extends AppCompatActivity {

    private Button btnCloseFalse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_false);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int popW = dm.widthPixels;
        int popH = dm.heightPixels;
        getWindow().setLayout((int)(popW*.8), (int)(popH*.4));
        getWindow().setGravity(1);
        btnCloseFalse = findViewById(R.id.btnCloseFalse);

    }


    public void onClick(View view){
        finish();
    }
}