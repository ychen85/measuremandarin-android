package com.example.measuremandarin;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    EditText edtUsername;
    EditText edtPassword;
    EditText edtRePassword;
    Button btnAlready;
    Button btnReg;
    LoginDatabase loginDB;
    private AccountState accountState;

//    public static final String USER_KEY = "account";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loginDB = LoginDatabase.getInstance(this);
        accountState = AccountState.getInstance(this);

        edtUsername = findViewById(R.id.edtUsername);
        edtPassword = findViewById(R.id.edtPassword);
        edtRePassword = findViewById(R.id.edtRePassword);
        btnAlready = findViewById(R.id.btnAlready);
        btnReg = findViewById(R.id.btnReg);
 //       loginDB = new LoginDatabase(this);

        btnAlready.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });


        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = edtUsername.getText().toString();
                String password = edtPassword.getText().toString();
                String repassword = edtRePassword.getText().toString();

                if(TextUtils.isEmpty(username) || TextUtils.isEmpty(password) || TextUtils.isEmpty(repassword)){
                    Toast.makeText(MainActivity.this, "all fileds required",Toast.LENGTH_SHORT).show();
                }else{
                    if (password.equals(repassword)){
                        boolean checkUser = loginDB.checkUsername(username);
                        if(checkUser == false){
                            boolean insert = loginDB.insertData(username, password);


                            if (insert == true){
                                User cur = new User(password,username);
                                accountState.getSavedUsers().add(cur);
                                accountState.setActiveUser(cur);
                                //create data in quizRecord
                                Date date = Calendar.getInstance().getTime();
                                DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
                                String strDate = dateFormat.format(date);
                                loginDB.saveResult(strDate,accountState.getActiveUser().getUsername(),"Registered","n/a", "0");
                                System.out.println("@@@@@@@@@@");
                                Toast.makeText(MainActivity.this,"Register Successfully!",Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(),AccountActivity.class);
                                startActivity(intent);
                            }else{
                                Toast.makeText(MainActivity.this,"Registeration Failed",Toast.LENGTH_SHORT).show();
                            }

                        }else{
                            Toast.makeText(MainActivity.this,"User already Exists!",Toast.LENGTH_SHORT).show();
                        }

                    }else{
                        Toast.makeText(MainActivity.this,"Confirm password again",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }
}
//        //trying to get JSON string
//        InputStream is = getResources().openRawResource(R.raw.users);
//        Writer writer = new StringWriter();
//        char[] buffer = new char[1024];
//        try {
//            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
//            int n;
//            while ((n = reader.read(buffer)) != -1) {
//                writer.write(buffer, 0, n);
//            }
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                is.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//
//        //String version of JSON file to pass to accountstate
//        String jsonString = writer.toString();


//        askReg = findViewById(R.id.toRegGreeting);
//        newUserGreeting = findViewById(R.id.textView);
//    }

//    //check login info
//    public void onLoginClick(View view) {
//        username = edtUsername.getText().toString();
//        password = edtPassword.getText().toString();
//
//        if(username.equals("") || password.equals("")){
//            Toast.makeText(this, "Please enter a username and password!", Toast.LENGTH_SHORT).show();
//            return;
//        }
//        // TODO: 24/10/2021 once verified , start new activity, display account information.
//        if(accountState.verify(username, password)){
//            Toast.makeText(this, "Successful Login", Toast.LENGTH_SHORT).show();
//        }else{
//            Toast.makeText(this, "Username/Password not found", Toast.LENGTH_SHORT).show();
//        }
//        Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
//        startActivity(intent);
//
//    }
//
//    public void onGoRegClick(View view){
//        btnToReg = findViewById(R.id.btnToReg);
//        Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
//        startActivity(intent);
//    }
//
//
//    public void onToRegClick(View view){
//            Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
//            startActivity(intent);
//    }


//    public void onLogInBtnClick (View view){
//    }

//    @Override
//    public void onClick(View v) {
//        Intent intent;
//        switch (v.getId()) {
//
//            case R.id.btn_tologin:
//                // code for button when user clicks buttonOne.
//                intent = new Intent(this, RegSuccessActivity.class);
//                startActivity(intent);
//
//                break;
//
//            case R.id.btn_regnow:
//                intent = new Intent(this, RegActivity.class);
//                startActivity(intent);
//                break;
//        }
//    }

//    public void goToReg (){
//        Intent intent = new Intent(this, RegisterActivity.class);
//        startActivity(intent);
//    }

//    public void showRegData(){
//        showName = findViewById(R.id.regusername);
//        showPassword = findViewById(R.id.regpassword);
//        showName.setText("Hello ! " + userName.getText().toString());// take the user input set in TextView
//        assignPassword = passwordGenerator();
//        showPassword.setText(assignPassword);
//    }


//    public String passwordGenerator(){
//        Random random = new Random();
//        int passwordA = random.nextInt(9);
//        int passwordB = random.nextInt(9);
//        int passwordC = random.nextInt(9);
//        int passwordD = random.nextInt(9);
//
//        assignPassword = initialPassword;
//
//        assignPassword += String.valueOf(passwordA);
//        assignPassword += String.valueOf(passwordB);
//        assignPassword += String.valueOf(passwordC);
//        assignPassword += String.valueOf(passwordD);
//
//        return assignPassword;
//    }

// //   boolean isEmpty(EditText text){
//        CharSequence strText = text.getText().toString();
//        return TextUtils.isEmpty(strText);
//    }
//
//    boolean isEmail(EditText text) {
//        CharSequence email = text.getText().toString();
//        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
//    }
//
//    //check input and provide warning
//    public void checkDataEntered(){
//        if (isEmpty(password)) {
//            Toast t = Toast.makeText(this, "Need first name to register", Toast.LENGTH_SHORT);
//            t.show();
//        }
//
//        if (isEmail(email) == false) {
//            email.setError("Please enter valid email!");
//        }
//    }

