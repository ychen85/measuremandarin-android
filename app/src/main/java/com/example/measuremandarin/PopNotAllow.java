package com.example.measuremandarin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;

public class PopNotAllow extends AppCompatActivity {
    private Button btnClose;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_not_allow);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int popW = dm.widthPixels;
        int popH = dm.heightPixels;
        getWindow().setLayout((int)(popW*.8), (int)(popH*.4));
        getWindow().setGravity(1);
        btnClose = findViewById(R.id.btnClose);

    }


    public void onClick(View view){
        finish();
    }
}