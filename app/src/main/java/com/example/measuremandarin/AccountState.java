package com.example.measuremandarin;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.io.FileNotFoundException;
import java.io.FileReader;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class AccountState {

    private static AccountState instance;
    private ArrayList<User> savedUsers;
    private User activeUser;
    private boolean easyDone;
    private boolean medDone;
    private boolean hardDone;


    public static AccountState getInstance(Context context) {
        if (instance == null) {
            instance = new AccountState(context);
        }
        return instance;
    }

    private AccountState(Context context) {
        this.savedUsers = new ArrayList<>();


    }

    public ArrayList<User> getSavedUsers() {
        return savedUsers;
    }

    public void setSavedUsers(ArrayList<User> savedUsers) {
        this.savedUsers = savedUsers;
    }

    public User getActiveUser() {
        return activeUser;
    }

    public void setActiveUser(User activeUser) {
        this.activeUser = activeUser;
    }

    public boolean isEasyDone() {
        return easyDone;
    }

    public void setEasyDone(boolean easyDone) {
        this.easyDone = easyDone;
    }

    public boolean isMedDone() {
        return medDone;
    }

    public void setMedDone(boolean medDone) {
        this.medDone = medDone;
    }

    public boolean isHardDone() {
        return hardDone;
    }

    public void setHardDone(boolean hardDone) {
        this.hardDone = hardDone;
    }
}


