package com.example.measuremandarin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class LessonEnterActivity extends AppCompatActivity {

    private TextView txtIntro;
    private TextView txtIntroContent;
    private Button btnStart;
    Button btnLessonEL;
    Button btnLessonEQ;
    Button btnLessonEA;
    Button btnLessonEO;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_enter);

        txtIntro = findViewById(R.id.txtIntro);
        txtIntroContent = findViewById(R.id.txtIntroContent);
        btnStart = findViewById(R.id.btnStart);
        btnLessonEL = findViewById(R.id.btnLessonEL);
        btnLessonEQ = findViewById(R.id.btnLessonEQ);
        btnLessonEA = findViewById(R.id.btnLessonEA);
        btnLessonEO = findViewById(R.id.btnLessonEO);



    }


    public void onClick(View view){
        Intent intent;
        switch(view.getId()){

            case R.id.btnLessonEL:
                intent = new Intent(LessonEnterActivity.this,LessonEnterActivity.class);
                startActivity(intent);
                break;

            case R.id.btnLessonEQ:
                intent = new Intent(LessonEnterActivity.this,QuizListActivity.class);
                startActivity(intent);
                break;

            case R.id.btnLessonEA:
                intent = new Intent(LessonEnterActivity.this,AccountActivity.class);
                startActivity(intent);
                break;

            case R.id.btnLessonEO:
                intent = new Intent(LessonEnterActivity.this,MainActivity.class);
                startActivity(intent);
                break;


            case R.id.btnStart:
                intent = new Intent(LessonEnterActivity.this, LessonListActivity.class);
                startActivity(intent);
        }
    }



}